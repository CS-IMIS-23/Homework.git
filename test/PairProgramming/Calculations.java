package PairProgramming;

import java.util.Stack;
import java.util.Random;
import java.util.ArrayList;
import java.util.Scanner;
class Questions {
    ArrayList<Object> array = new ArrayList<Object>();
    Random generator = new Random();
    char[] newchar = {'+', '-', '*', '/'};
    protected int number;
    int NUM;
    public Questions() {
        number = 0;
    }
    public Object getQuestion(int num) {
        int num1 = num;

        while (num > 0) {
            int figure = (int) generator.nextInt(9) + 1;
            array.add(figure);
            number = (int) (Math.random() * 4);
            array.add(newchar[number]);
            num--;
        }
        String obj = "";
        while (num < 2 * num1) {
            obj += array.get(num);
            num++;
        }
        int other = (int) generator.nextInt(9) + 1;
        array.add(other);
        obj += other + "=";

        return obj;
    }
}//----------生成题目------------------
public class Calculations {
    public static void main(String[] args) {
        Questions questions=new Questions();
        Stack stack = new Stack();

        Scanner Scan=new Scanner(System.in);

        char c;
        int count=0,answer;
        char[] operation = new char[100];
        String str = (String) questions.getQuestion(3);

        System.out.println("请回答以下问题：\n"+str);
        System.out.println("请输入你的答案：");
        answer=Scan.nextInt();

        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);
            if (c >= '0' && c <= '9') {

                operation[i] = c;
                count++;

               }
            else {

                if (c == '*' || c == '/') {
                    if (stack.empty()) {
                        stack.push((char) c);
                    } else if ((char) stack.peek() == '*' || (char) stack.peek() == '/') {
                        operation[i] = (char) stack.pop();
                        stack.push(c);
                    } else
                        stack.push(c);
                } else if (c == '+' || c == '-') {
                    if (stack.empty()) {
                        stack.push(c);
                    } else if ((char) stack.peek() == '+' || (char) stack.peek() == '-') {
                        operation[i] = (char) stack.pop();
                        stack.push(c);
                    } else {
                        operation[i] = (char) stack.pop();
                        stack.push(c);
                    }

                } else
                    stack.push(c);

            }
        }
        int num = stack.size();
        for (int a = 0; a < num; a++) {
            operation[str.length() + a] = (char) stack.pop();
        }
        //------------题目转后缀表达式------------------------

       // System.out.println(operation);
      //  System.out.println(stack);

    Stack<Integer> stack1 = new Stack<Integer>();

    int m, n, sum,num1=str.length()+(str.length()-count);

        for (int b = 0; b <= num1; b++) {
        if (operation[b] >= '0' && operation[b] <= '9')
            stack1.push((int) operation[b]-48);
        else {
            if (operation[b] == '+') {
                m =  stack1.pop();
                n =  stack1.pop();
                sum = n + m;
                stack1.push(sum);
            } else if (operation[b] == '-') {
                m = stack1.pop();
                n = stack1.pop();
                sum = n- m;
                stack1.push(sum);
            } else if (operation[b] == '*') {
                m = stack1.pop();
                n = stack1.pop();
                sum = n * m;
                stack1.push(sum);
            } else if (operation[b] == '/') {
                m =  stack1.pop();
                n =  stack1.pop();
                sum = n / m;
                stack1.push(sum);
            }
            else if (operation[b] == ' ')
                continue;
        }
    }
      //  System.out.println(stack1);
        if ((int)stack1.peek()==answer)
            System.out.println("恭喜你答对了！");
        else
            System.out.println("很遗憾，答错了！答案是："+stack1.peek());
}
}

