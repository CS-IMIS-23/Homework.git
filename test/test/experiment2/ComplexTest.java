package test.experiment2; 

import experiment2.Complex;
import org.junit.Test;
import static org.junit.Assert.*;

public class ComplexTest {
    Complex a=new Complex(1,2);
    Complex b=new Complex(-1,3);

@Test
public void testAdd() throws Exception {
    assertEquals("(0.0+5.0i)",String.valueOf(a.add(b)));
} 

@Test
public void testMinus() throws Exception {
    assertEquals("(2.0-1.0i)",String.valueOf(a.minus(b)));
}
@Test
public void testMultiply() throws Exception {
    assertEquals("(-7.0+1.0i)",String.valueOf(a.multiply(b)));
}
@Test
public void testDivide() throws Exception {
    assertEquals("(1.25+1.0i)",String.valueOf(a.divide(b)));
}

@Test
public void testToString() throws Exception { 
assertEquals("(1.0+2.0i)",String.valueOf(a));
} 


} 
