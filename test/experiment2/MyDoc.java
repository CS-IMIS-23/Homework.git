package experiment2;
// Server Classes
abstract class Data {
    abstract public void DisplayValue();
}
class Boolean extends Data {
    boolean value;
    Boolean() {
        value = true;
    }
    public void DisplayValue(){
        System.out.println (value);
    }
}
// Pattern Classes
abstract class Factory {
    abstract public Data CreateDataObject();
}
class BooleanFactory extends Factory{
    public Data CreateDataObject(){
        return new Boolean();
    }
}
//Client classes
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class.
public class MyDoc {
    static Document d;
    public static void main(String[] args) {
        d = new Document(new BooleanFactory());
        d.DisplayData();
    }
}