package experiment2;

public class Complex {
    private double R;// 实部
    private double I;// 虚部

    public Complex(double R, double I) {
        this.R = R;
        this.I = I;
    }
    // add
    public Complex add(Complex c) {
        return new Complex(R + c.R, I + c.I);
    }

    // minus
    public Complex minus(Complex c) {
        return new Complex(R - c.R, I - c.I);
    }

    // multiply
    public Complex multiply(Complex c) {
        return new Complex(R * c.R - I* c.I, R * c.I +I * c.R);
    }

    // divide
    public Complex divide(Complex c) {
        double d = Math.sqrt(c.R * c.R) + Math.sqrt(c.I * c.I);
        return new Complex((R * c.R + I * c.I) / d, Math.round((R * c.I - I * c.R) / d));
    }

    public String toString() {
        String rtr_str = "";
        if (I > 0)
            rtr_str = "(" +R + "+" + I+ "i" + ")";
        if (I == 0)
            rtr_str = "(" +R + ")";
        if (I < 0)
            rtr_str = "(" + R + I + "i" + ")";
        return rtr_str;
    }
}
