package experiment3;

class RefactorEmployee
{
    private String name ;
    private int age ;
    private int ID;
    private boolean Worker;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "RefactorEmployee{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", ID=" + ID +
                ", Worker=" + Worker +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public boolean isWorker() {
        return Worker;
    }

    public void setWorker(boolean worker) {
        Worker = worker;
    }
}

public class RefactorEmployeeTest {
    public static void main(String[] args) {
        RefactorEmployee employee = new RefactorEmployee();
        employee.setAge(30);
        employee.setID(2);
        employee.setName("w");
        employee.setWorker(true);
        System.out.println("Name: "+employee.getName());
        System.out.println(employee);
    }
}

