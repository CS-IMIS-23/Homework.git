package experiment3;


public class Complex
{
    private double RealPart;
    private double ImagePart;

    public Complex(double R,double I)
    {
        RealPart = R;
        ImagePart = I;
    }

    public boolean equals(Complex obj)
    {
        boolean result = false;
        if (RealPart ==obj.RealPart && ImagePart==obj.ImagePart)
            result = true;
        return result;
    }


    public Complex ComplexAdd(Complex obj)
    {
        RealPart +=obj.RealPart;
        ImagePart +=obj.ImagePart;
        return new Complex(RealPart,ImagePart);
    }

    public Complex ComplexSub(Complex obj)
    {
        RealPart -=obj.RealPart;
        ImagePart -=obj.ImagePart;
        return new Complex(RealPart,ImagePart);
    }

    public Complex ComplexMulti(Complex obj)
    {
        double realpart = RealPart;
        RealPart = RealPart*obj.RealPart-ImagePart*obj.ImagePart;
        ImagePart = realpart*obj.ImagePart+ImagePart*obj.RealPart;
        return new Complex(RealPart,ImagePart);
    }

    public Complex ComplexDiv(Complex obj)
    {
        Complex number = new Complex(RealPart,ImagePart);
        Complex number2 = new Complex(RealPart,ImagePart);
        Complex conjugateobj = new Complex(obj.RealPart,0-obj.ImagePart);
        Double denominator = (obj.ComplexMulti(conjugateobj)).RealPart;
        RealPart = (number.ComplexMulti(conjugateobj).RealPart)/denominator;
        ImagePart = ((number2.ComplexMulti(conjugateobj)).ImagePart)/denominator;

        return new Complex(RealPart,ImagePart);
    }
    public String toString()
    {
        String result="";

        if(ImagePart == 0.0)
            result = RealPart+"";
        else
        if(RealPart==0.0)
            result = ImagePart+"i";
        else
        if(ImagePart>0)
            result = RealPart+""+"+"+ImagePart+"i";
        else
            result = RealPart+""+ImagePart+"i";

        return result;
    }

}

