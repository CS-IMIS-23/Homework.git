package chapter11;

import chapter11.TextCode.LinkedBinarySearchTree;

public class PP113Test {
    public static void main(String[] args) {
        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree();
        linkedBinarySearchTree.addElement(1);
        linkedBinarySearchTree.addElement(2);
        linkedBinarySearchTree.addElement(3);
        linkedBinarySearchTree.addElement(4);
        linkedBinarySearchTree.addElement(5);

        System.out.println("最大值为："+linkedBinarySearchTree.findMax());
        System.out.println("最小值为："+linkedBinarySearchTree.findMin());
        linkedBinarySearchTree.removeMax();
        System.out.println("删除最大值后："+linkedBinarySearchTree);
    }
}
