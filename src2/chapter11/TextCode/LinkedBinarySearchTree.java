package chapter11.TextCode;

import chapter10.TextCode.BinaryTreeNode;
import chapter10.TextCode.LinkedBinaryTree;
import chapter11.TextCode.exceptions.ElementNotFoundException;
import chapter11.TextCode.exceptions.EmptyCollectionException;
import chapter11.TextCode.exceptions.NonComparableElementException;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月24日 下午2:10:17
 * @description 类说明
 */
public class LinkedBinarySearchTree<T> extends LinkedBinaryTree<T> implements BinarySearchTreeADT<T> {

	/**
	 * Creates an empty binary search tree.
	 */
	public LinkedBinarySearchTree() {
		super();
	}

	/**
	 * Creates a binary search with the specified element as its root.
	 *
	 * @param element
	 *            the element that will be the root of the new binary search
	 *            tree
	 */
	public LinkedBinarySearchTree(T element) {
		super(element);

		if (!(element instanceof Comparable))
			throw new NonComparableElementException("LinkedBinarySearchTree");
	}

	/**
	 * addElement根据方法给定的元素的值，在树中的恰当的位置添加该元素。如果这个元素不是Comparable,
	 * 则addElement方法会抛出一个NonComparableElementException("LinkedBinarySearchTree")
	 * 异常。如果树为空，那么新元素就会称为根结点，如果不为空，如果小于则 在左边递归；大于等于，则在右边递归。 元素添加算法的实现：
	 *
	 *
	 */
	@Override
	public void addElement(T element) {
		if (!(element instanceof Comparable)) {
			throw new NonComparableElementException("LinkedBinarySearchTree");
		}

		Comparable<T> comElem = (Comparable<T>) element;
		if (isEmpty()) {
			root = new BinaryTreeNode<T>(element);
		} else {
			if (comElem.compareTo(root.getElement()) < 0) {
				if (root.getLeft() == null) {
					this.getRootNode().setLeft(new BinaryTreeNode<T>(element));
				} else {
					addElement(element, root.getLeft());
				}
			} else {
				if (root.getRight() == null) {
					this.getRootNode().setRight(new BinaryTreeNode<T>(element));
				} else {
					addElement(element, root.getRight());
				}
			}
		}

		modCount++;
	}

	/**
	 * 将特定的元素添加到二叉查找树的合适位置
	 *
	 * @param element
	 * @param node
	 */
	private void addElement(T element, BinaryTreeNode<T> node) {
		Comparable<T> comparableElement = (Comparable<T>) element;

		if (comparableElement.compareTo(node.getElement()) < 0) {
			if (node.getLeft() == null)
				node.setLeft(new BinaryTreeNode<T>(element));
			else
				addElement(element, node.getLeft());
		} else {
			if (node.getRight() == null)
				node.setRight(new BinaryTreeNode<T>(element));
			else
				addElement(element, node.getRight());
		}

	}

	/**
	 * 负责从二叉查找树中删除给定的comparable元素；或者当在树中找不到给定的目标元素时，则抛出ElementNotFoundException
	 * 异常。与前面的线性结构研究不同，这里不能通过简单的通过删除指定结点的相关引用指针而删除该结点。相反这里必须推选出一个结点来取代
	 * 要被删除的结点。受保护方法，replacement返回指向一个结点的应用，该结点将代替要删除的结点，选择替换结点有三种情况：
	 * 如果被删除结点没有孩子那么repalcement返回null 如果有一个，那么replacement返回这个孩子
	 * 如果有两个孩子，则replacement会返回终须后继者。因为相等元素会放在后边
	 */
	@Override
	public T removeElement(T targetElement) {

		T result = null;
		if (isEmpty()) {
			throw new ElementNotFoundException("LinkedbinarySearchTree");
		} else {
			BinaryTreeNode<T> parent = null;
			if (((Comparable<T>) targetElement).equals(root.getElement())) {
				result = root.element;
				BinaryTreeNode<T> temp = replacement(root);

				if (temp == null) {
					root = null;
				} else {

					root.element = temp.element;
					root.setLeft(temp.getLeft());
					root.setRight(temp.getRight());

				}
				modCount--;

			} else {
				parent = root;
				if (((Comparable<T>) targetElement)
						.compareTo(root.getElement()) < 0) {
					result = removeElement(targetElement, root.getLeft(),
							parent);
				} else {
					result = removeElement(targetElement, root.getRight(),
							parent);
				}
			}

		}

		return result;
	}

	private T removeElement(T targetElement, BinaryTreeNode<T> node,
							BinaryTreeNode<T> parent) {
		T result = null;

		if (node == null) {
			throw new ElementNotFoundException("LinkedbinarySearchTree");
		} else {

			if (((Comparable<T>) targetElement).equals(node.getElement())) {
				result = node.element;
				BinaryTreeNode<T> temp = replacement(node);

				// 看当前应该删除的降低ian
				if (parent.right == node) {
					parent.right = temp;
				} else {
					parent.left = temp;
				}

				modCount--;

			} else {
				parent = node;
				if (((Comparable<T>) targetElement)
						.compareTo(root.getElement()) < 0) {
					result = removeElement(targetElement, root.getLeft(),
							parent);
				} else {
					result = removeElement(targetElement, root.getRight(),
							parent);
				}
			}

		}

		return result;
	}

	private BinaryTreeNode<T> replacement(BinaryTreeNode<T> node) {
		BinaryTreeNode<T> result = null;
		if ((node.left == null) && (node.right == null)) {
			result = null;
		} else if ((node.left != null) && (node.right == null)) {
			result = node.left;

		} else if ((node.left == null) && (node.right != null)) {
			result = node.right;
		} else {
			BinaryTreeNode<T> current = node.right;// 初始化右侧第一个结点
			BinaryTreeNode<T> parent = node;

			// 获取右边子树的最左边的结点
			while (current.left != null) {
				parent = current;
				current = current.left;
			}

			current.left = node.left;

			// 如果当前待查询的结点
			if (node.right != current) {
				parent.left = current.right;// 整体的树结构移动就可以了
				current.right = node.right;
			}

			result = current;
		}
		return result;
	}

	/**
	 * 负责从二叉查找树中删除制定元素的所有存在;或者，当先在树中找不到指定元素的时候，抛出ElementNotFoundException
	 * 异常。如果指定元素不是com类型 则抛出class异常。
	 */
	@Override
	public void removeAllOccurrences(T targetElement) {
		removeElement(targetElement);

		try {
			while (contains((T) targetElement))
				removeElement(targetElement);
		}

		catch (Exception ElementNotFoundException) {
		}

	}

	@Override
	public T removeMin() {
		T result = null;

		if (isEmpty())
			throw new EmptyCollectionException("LinkedBinarySearchTree");
		else {
			if (root.left == null) {
				result = root.element;
				root = root.right;
			} else {
				BinaryTreeNode<T> parent = root;
				BinaryTreeNode<T> current = root.left;
				while (current.left != null) {
					parent = current;
					current = current.left;
				}
				result = current.element;
				parent.left = current.right;
			}

			modCount--;
		}
		return result;
	}

	@Override
	public T removeMax() {
		T result = null;

		if (isEmpty())
			throw new EmptyCollectionException("LinkedBinarySearchTree");
		else {
			if (root.right == null) {
				result = root.element;
				root = root.left;
			} else {
				BinaryTreeNode<T> parent = root;
				BinaryTreeNode<T> current = root.right;
				while (current.right != null) {
					parent = current;
					current = current.right;
				}
				result = current.element;
				parent.right = current.left;
			}

			modCount--;
		}

		return result;
	}

	@Override
	public T findMin() {
		T result = null;

		if (isEmpty())
			throw new EmptyCollectionException("LinkedBinarySearchTree");
		else {
			if (root.left == null) {
				result = root.element;
				//root = root.right;
			} else {
				BinaryTreeNode<T> parent = root;
				BinaryTreeNode<T> current = root.left;
				while (current.left != null) {
					parent = current;
					current = current.left;
				}
				result = current.element;
				//parent.left = current.right;
			}

			//modCount--;
		}

		return result;
	}

	@Override
	public T findMax() {
		T result = null;

		if (isEmpty())
			throw new EmptyCollectionException("LinkedBinarySearchTree");
		else {
			if (root.right == null) {
				result = root.element;
				//root = root.left;
			} else {
				BinaryTreeNode<T> parent = root;
				BinaryTreeNode<T> current = root.right;
				while (current.right != null) {
					parent = current;
					current = current.right;
				}
				result = current.element;
				//parent.right = current.left;
			}

			//modCount--;
		}

		return result;
	}

}



