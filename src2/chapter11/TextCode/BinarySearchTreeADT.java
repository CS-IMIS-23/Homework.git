package chapter11.TextCode;

import chapter10.TextCode.BinaryTreeADT;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月24日 上午11:04:13 
 * @description 类说明
 */
public interface BinarySearchTreeADT<T> extends BinaryTreeADT<T> {

	//添加自身具有的方法
	/**
	 * 往树中添加一个元素
	 * @param element
	 */
	public void addElement(T element);
	/**
	 * 树中删除一个元素
	 * @param element
	 */
	public T removeElement(T element);
	/**
	 * 往树中删除元素的任何存在
	 * @param element
	 */
	public void removeAllOccurrences(T targetElement);
	/**
	 * 删除最小的元素
	 * @param element
	 */
    public T removeMin();
    /**
	 * 删除最大的元素
	 * @param element
	 */
    public T removeMax();
    /**  
	 * @param element
	 */
    public T findMin();
    /**
	 * 返回一个指向树中最大元素的引用
	 * @param element
	 */
    public T findMax();

}
