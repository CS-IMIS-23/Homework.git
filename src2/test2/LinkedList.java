package test2;

import chapter13.Magazine;
import chapter13.MagazineList;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class LinkedList {
        public static void main(String[] args) throws IOException {
            /*-------------------------------------------------
                                   实验一
             ---------------------------------------------------*/

            int nzhouyajie = 0;//记录链表元素总数
            System.out.println("请输入数据：");
            Scanner scan = new Scanner(System.in);
            String value = scan.nextLine();//value存储输入台输入的字符串
            StringTokenizer st = new StringTokenizer(value);

            MagazineList numberNode = new MagazineList();//创建链表numbernode
            if (st.hasMoreTokens()) {//将输入的字符串按空白符查找

                while (st.hasMoreTokens()) {
                    String str = st.nextToken();//将下一个相邻两个空白符间的字符串赋给str
                    numberNode.add(new Magazine(str));//将str添加到链表
                    nzhouyajie++;//链表元素加1
                }
            } else
                System.out.println("未输入数据");

            System.out.println("链表元素：" + numberNode);
            System.out.println("一共有" + nzhouyajie + "个元素");
            /*-------------------------------------------------
                                   实验二
             ---------------------------------------------------*/

            File file = new File("D:\\Code", "sort.txt");

            if (!file.exists()) {
                file.createNewFile();
            }
            OutputStream outputStream1 = new FileOutputStream(file);//--------创建文件

            outputStream1.write(("1 2").getBytes());//将1 2写入文件
            InputStream inputStream1 = new FileInputStream(file);//读取文件中数字
            String result = new BufferedReader(new InputStreamReader(inputStream1))
                    .lines().collect(Collectors.joining(System.lineSeparator()));//将数字付给string

            StringTokenizer ST = new StringTokenizer(result);//将数字分隔开

            numberNode.insert(4, new Magazine(ST.nextToken()));//读取数字1插入到链表第五位
            System.out.println("链表元素：" + numberNode);
            System.out.println("一共有" + (nzhouyajie+1) + "个元素");

            numberNode.insert(0, new Magazine(ST.nextToken()));//读取数字2插入到第0位
            System.out.println("链表元素：" + numberNode);
            System.out.println("一共有" + (nzhouyajie+2) + "个元素");

            numberNode.delete(new Magazine("1"));//删除数字1
            System.out.println("链表元素：" + numberNode);
            System.out.println("一共有" + (nzhouyajie+1) + "个元素");

            inputStream1.close();//------------文件读取-------
            /*-------------------------------------------------
                                   实验三
             ---------------------------------------------------*/
            numberNode.contact();

            /*-------------------------------------------------
                                   实验四
             ---------------------------------------------------*/




}
}

