package test2;

import chapter10.Sorting;
import chapter13.Magazine;

import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class ArrayList {
    public static void main(String[] args) throws IOException {
        String[] number = new String[100];
        int nzhouyajie = 0;//记录链表元素总数
        System.out.println("请输入数据：");
        Scanner scan = new Scanner(System.in);
        String value = scan.nextLine();//value存储输入台输入的字符串
        StringTokenizer st = new StringTokenizer(value);

        if (st.hasMoreTokens()) {//将输入的字符串按空白符查找
            for (nzhouyajie = 0; st.hasMoreTokens(); nzhouyajie++) {
                String str = st.nextToken();//将下一个相邻两个空白符间的字符串赋给str
                number[nzhouyajie] = str;//将str添加到链表
            }
        } else
            System.out.println("未输入数据");
        System.out.println("链表元素：");
        for (int count = 0; count < number.length; count++) {
            if (number[count] != null)
                System.out.print(number[count] + " ");
            else
                break;//不输出数组中为赋值的索引
        }
        System.out.println();
        System.out.println("一共有" + nzhouyajie + "个元素");
        //-------------------------------------------------
        File file = new File("D:\\Code", "read.txt");

        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);//--------创建文件

        outputStream1.write(("1 2").getBytes());//将1 2写入文件
        InputStream inputStream1 = new FileInputStream(file);//读取文件中数字
        String result = new BufferedReader(new InputStreamReader(inputStream1))
                .lines().collect(Collectors.joining(System.lineSeparator()));//将数字付给result

        StringTokenizer ST = new StringTokenizer(result);//将数字分隔开

        //--------------------------------------------------------
        for (int x = nzhouyajie; x >= 4; x--) {
            number[x + 1] = number[x];
        }
        number[4] = ST.nextToken();//读取数字1插入到链表第五位
        nzhouyajie++;
        System.out.println("链表元素（加入数字1）:");
        for (int count = 0; count < number.length; count++) {
            if (number[count] != null)
                System.out.print(number[count] + " ");
            else
                break;//不输出数组中为赋值的索引
        }
        System.out.println();
        System.out.println("一共有" + (nzhouyajie) + "个元素");

        int n1 = 0; //保存元素个数的变量
        for (int i = 0; i < number.length; i++) {
            if (null != number[i])
                n1++;
        }
        for (int s = 0;s<=n1;n1--){
            number[n1+1]=number[n1];
        }
        number[0]=ST.nextToken();
        System.out.println("链表元素（加入数字2）：");
        for (int count = 0; count < number.length; count++) {
            if (number[count] != null)
                System.out.print(number[count] + " ");
            else
                break;//不输出数组中为赋值的索引
        }
        nzhouyajie++;
        System.out.println();
        System.out.println("一共有" + (nzhouyajie) + "个元素");
        //-------------------------------------------------
        int n2 = 0; //保存元素个数的变量
        for (int i = 0; i < number.length; i++) {
            if (null != number[i])
                n2++;
        }
        for (int a = 4; a <= n2; a++) {//删除索引4处的数字1
            number[a] = number[a + 1];
        }
        nzhouyajie--;
        for (int count = 0; count < number.length; count++) {
            if (number[count] != null)
                System.out.print(number[count] + " ");
            else
                break;//不输出数组中为赋值的索引
        }
        System.out.println();
        System.out.println("一共有" + (nzhouyajie) + "个元素");

        for (int index = 1; index <nzhouyajie; index++)//插入排序
        {
            String key = number[index];
            int position = index;

            //  Shift larger values to the right
            while (position > 0 && Integer.parseInt(key)<Integer.parseInt(number[position-1]))
            {
                number[position] = number[position-1];
                position--;
            }
            number[position] = key;
            System.out.println("排序后链表元素：" );
            for (int i = 0; i < nzhouyajie; i++) {
                System.out.print(number[i] + " ");
            }

            System.out.println("排序后元素个数为：" + nzhouyajie);
        }
    }
}

