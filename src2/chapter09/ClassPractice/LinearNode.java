package chapter09.ClassPractice;


public class LinearNode<T> {
    protected LinearNode<T> next;
    protected int count;
    protected LinearNode<T> head;
    private T element;


    public LinearNode() {
        next = null;
        element = null;
    }


    public LinearNode(T elem) {
        next = null;
        element = elem;
    }

    public LinearNode<T> getNext() {
        return next;
    }

    public void setNext(LinearNode<T> node) {
        next = node;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T elem) {
        element = elem;
    }
    public String toString1()
    {
        int n = count;
        String result = "[";
        LinearNode<T> current = head;
        while(n > 0){
            result += current.getElement();
            current = current.getNext();
            n--;
        }
        return result+"]";
    }
}

