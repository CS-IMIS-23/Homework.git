package chapter09.PP93;

import java.util.Calendar;

public class SortAlgorithm2Test {
    public static void main(String[] args) {
        SortAlgorithm2 sortAlgorithm2 = new SortAlgorithm2();
        Integer[] array = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};

        Integer[] array2 = {23,12,34,56,23,1,434,6,434,32,65,7,54,34,453,56,67,76,44,56};

        sortAlgorithm2.selectionSort(array);
        sortAlgorithm2.selectionSort(array2);
        System.out.println();
        sortAlgorithm2.insertionSort(array);
        sortAlgorithm2.insertionSort(array2);

        System.out.println();
        sortAlgorithm2.bubbleSort(array);
        sortAlgorithm2.bubbleSort(array2);

        System.out.println();
        sortAlgorithm2.quickSort(array);
        sortAlgorithm2.quickSort(array2);

        System.out.println();
        sortAlgorithm2.MergeSort(array);
        sortAlgorithm2.MergeSort(array);


    }
}
