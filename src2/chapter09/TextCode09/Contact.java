package chapter09.TextCode09;

public class Contact implements Comparable<Contact> {

	private String firstName;
	private String lastName;
	private String phone;
	
	public Contact(){
		
	}
	public Contact(String firstName, String lastName, String phone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.phone = phone;
	}


	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.firstName+","+this.lastName+":"+this.phone;
	}
	
	@Override
	public int compareTo(Contact o) {
		
		if(firstName.equals(o.firstName)){
			return lastName.compareTo(o.lastName);
		}else{
			return firstName.compareTo(o.firstName);
		}
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	
	

}
