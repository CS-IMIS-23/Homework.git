package chapter09.TextCode09;

public class Searching {

	/**
	 * 线性查找 在没有找到之前 需要一直遍历
	 */
	public static <T extends Comparable<T>> boolean linearSearch(T[] data,
			int min, int max, T target) {

		int index = min;
		boolean found = false;

		while (!found && index <= max) {
			found = data[index].equals(target);
			index++;
		}

		return false;

	}

	/**
	 * 二分查找：二分查找需要实现数组列表有序，然后每次考察中间元素，排除一半，最好的方法是使用递归实现
	 */
	public static <T extends Comparable<? super T>> boolean binarySearch(T[] data, int min, int max, T target) {
		boolean flag= false;
		int mid = (max+min)/2;
		
		if(data[mid].compareTo(target)==0){
			flag = true;
		}else if(data[mid].compareTo(target)>0){//中间大于目标
			if(min<=mid-1){
				flag = binarySearch(data, min, mid-1, target);
			}
		}else if(data[mid].compareTo(target)<0){
			if(mid+1<=max){
				flag = binarySearch(data, mid+1, max, target);
			}
		}
		
		return flag;

	}

}
