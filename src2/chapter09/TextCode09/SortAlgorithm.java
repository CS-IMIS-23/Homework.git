package chapter09.TextCode09;

public class SortAlgorithm {

	/*
	 * 交换位置
	 */
	private static <T extends Comparable<? super T>> void swap(T[] data, int i,
			int j) {
		T temp = data[i];
		data[i] = data[j];
		data[j] = temp;
	}

	/**
	 * 选择排序：
	 */
	public static <T extends Comparable<? super T>> void selectionSort(T[] data) {
		int min;
		T temp;
		for (int i = 0; i < data.length - 1; i++) {
			min = i;
			// /找出最小的下标
			for (int j = i + 1; j < data.length; j++) {
				if (data[min].compareTo(data[j]) > 0) {
					min = j;
				}
			}
			// 交换位置
			swap(data, min, i);
		}

	}

	/**
	 * 插入排序:
	 */
	public static <T extends Comparable<? super T>> void insertionSort(T[] data) {

		// 从第一个开始比较
		for (int i = 1; i < data.length; i++) {
			T key = data[i];
			int pos = i;//
			// /有序列表 当前这个元素是否需要前移 如果当前的待插入元素比有序列表pos位置的小
			while (pos > 0 && data[pos - 1].compareTo(key) > 0) {
				data[pos] = data[pos - 1];
				pos--;
			}
			data[pos] = key;
		}

	}

	/**
	 * 冒泡排序：
	 */
	public static <T extends Comparable<? super T>> void bubbleSort(T[] data) {

		/**
		 * for(int i=1;i<data.length;i++){ ///已经有序的部分 for(int j
		 * =0;j<data.length-i;j++){ if(data[j].compareTo(data[j+1])>0){
		 * swap(data,j,j+1); }
		 * 
		 * }
		 * 
		 * }
		 */
		int pos;
		int scan;

		for (pos = data.length - 1; pos >= 0; pos--) {
			for (scan = 0; scan < pos; scan++) {
				if (data[scan].compareTo(data[scan + 1]) > 0) {
					swap(data, scan, scan + 1);
				}
			}
		}

	}

	/**
	 * 快速排序：
	 */
	public static <T extends Comparable<? super T>> void quickSort(T[] data) {
		quickSortRec(data, 0, data.length - 1);
	}

	/**
	 * 需要使用递归的快速排序
	 */
	private static <T extends Comparable<? super T>> void quickSortRec(
			T[] data, int begin, int end) {
		if (begin < end) {
			// int mid = AdjustArray(data, begin, end);//先成挖坑填数法调整data[]
			int mid = partition(data, begin, end);// 先成挖坑填数法调整data[]
			quickSortRec(data, begin, mid - 1); // 递归调用
			quickSortRec(data, mid + 1, end);
		}

	}

	/**
	 * 挖坑填数的快速排序的实现
	 */
	private static <T extends Comparable<? super T>> int AdjustArray(T[] data,
			int begin, int end) {
		int i = begin, j = end;
		T x = data[begin]; // s[l]即s[i]就是第一个坑
		// /由于是从小到达的排序算法
		// /因此我们首先找一个比基准小的数放在当前的坑里
		while (i < j) {
			// 从右向左找小于x的数来填s[i] 这是找小于基准数的过程
			while (i < j && data[j].compareTo(x) > 0)
				j--;

			if (i < j) {
				data[i] = data[j]; // 将s[j]填到s[i]中，s[j]就形成了一个新的坑
				i++;
			}
			// ------------------------------------------------------------------------------//
			// 从左向右找大于或等于x的数来填s[j] 这是找大于基准数的过程
			while (i < j && data[i].compareTo(x) < 0)
				i++;
			if (i < j) {
				data[j] = data[i]; // 将s[i]填到s[j]中，s[i]就形成了一个新的坑
				j--;
			}
		}
		// 退出时，i等于j。将x填到这个坑中。
		data[i] = x;
		return i;

	}

	/**
	 * 第二种分割方法的实现
	 */
	private static <T extends Comparable<? super T>> int partition(T[] data,
			int min, int max) {

		T pa;
		int left, right;
		int mid = (min + max) / 2;
		// 将中点的data作为分割元素
		pa = data[mid];
		// 将分割元素前置到min处
		swap(data, mid, min);

		left = min;
		right = max;

		while (left < right) {
			while (left < right && data[left].compareTo(pa) <= 0) {
				left++;
			}
			while (data[right].compareTo(pa) > 0) {
				right--;
			}
			if (left < right)
				swap(data, left, right);
		}
		// 分割点置回中间位置
		swap(data, min, right);

		return right;
	}

	/**
	 * 归并排序：
	 */
	public static <T extends Comparable<? super T>> void mergeSort(T[] data,
			int min, int max) {
		if (min < max) {
			int mid = (min + max) / 2;
			mergeSort(data, min, mid);
			mergeSort(data, mid + 1, max);
			merge(data, min, mid, max);
		}

	}
	/**
     * Merges two sorted subarrays of the specified array.
     */
	private static <T extends Comparable<? super T>> void merge(T[] data,
			int first, int mid, int last) {
		
		T[] temp = (T[]) (new Comparable[data.length]);

		int first1 = first, last1 = mid; // endpoints of first subarray
		int first2 = mid + 1, last2 = last; // endpoints of second subarray
		
		//记录下标
		
		int index = first1; // next index open in temp array

		// Copy smaller item from each subarray into temp until one
		// of the subarrays is exhausted
		while (first1 <= last1 && first2 <= last2) {
			if (data[first1].compareTo(data[first2]) < 0) {
				temp[index] = data[first1];
				first1++;
			} else {
				temp[index] = data[first2];
				first2++;
			}
			index++;
		}

		// Copy remaining elements from first subarray, if any
		while (first1 <= last1) {
			temp[index] = data[first1];
			first1++;
			index++;
		}

		// Copy remaining elements from second subarray, if any
		while (first2 <= last2) {
			temp[index] = data[first2];
			first2++;
			index++;
		}

		// Copy merged data into original array
		for (index = first; index <= last; index++)
			data[index] = temp[index];
	}
}
