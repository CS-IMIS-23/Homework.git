package chapter09.TextCode09;

import java.util.LinkedList;
import java.util.Queue;
/*
 * @description 基数排序
 */
public class RadixSort {

	public static void main(String[] args) {
		int[] list = { 43, 4568, 8765, 6543, 7865, 4532, 987, 3241, 6589,
				6622, 11 };

		String temp;
		Integer numObj;
		int digit, num;

		// 队列的声明
		Queue<Integer>[] digitQueues = (LinkedList<Integer>[]) (new LinkedList[10]);

		// 初始化10个队列
		for (int digitVal = 0; digitVal <= 9; digitVal++)
			digitQueues[digitVal] = (Queue<Integer>) (new LinkedList<Integer>());

		// sort the list 四位数 处理
		for (int position = 0; position <= 3; position++) {
			for (int scan = 0; scan < list.length; scan++) {
				temp = String.valueOf(list[scan]);// /转化为字符串
				//System.out.println(temp+"*"+temp.length());
				if(temp.length()>position){
					digit = Character.digit(temp.charAt(temp.length()-1 - position), 10);//十进制
				}else{
					digit = Character.digit('0', 10);//十进制
				}
				
				digitQueues[digit].add(new Integer(list[scan]));// 队列
			}

			// gather numbers back into list  
			//整理数据还原到列表中
			num = 0;
			for (int digitVal = 0; digitVal <= 9; digitVal++) {
				while (!(digitQueues[digitVal].isEmpty())) {
					numObj = digitQueues[digitVal].remove();
					list[num] = numObj.intValue();
					num++;
				}
			}
		}

		// output the sorted list
		for (int scan = 0; scan < list.length; scan++)
			System.out.println(list[scan]);
	}

}
