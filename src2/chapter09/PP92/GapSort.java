package chapter09.PP92;

public class GapSort {
    public static <T extends Comparable<? super T>> void bubbleSort(T[] data) {

        int pos,scan;

        for (pos = data.length - 1; pos >= 0; pos--) {
            for (scan = 0; scan < pos; scan++) {
                if (data[scan].compareTo(data[scan + 1]) > 0) {
                    swap(data, scan, scan + 1);
                }
            }
        }
        for (int i = 0;i < data.length;i++){//打印数组
            System.out.print(data[i]+" ");
        }
    }
    public static <T extends Comparable<? super T>> void gapsort(T[] data) {

//        int i,scan;
//        for (i = data.length - 1;i>0;i--){
//        for (scan = 0; scan < data.length - 1; scan++) {
//            if (scan + i < data.length) {
//                if (data[scan].compareTo(data[scan + i]) > 0)
//                    swap(data, scan, scan + i);
//            }
//
//        }
//        }
        int i,scan;
        for (i = data.length - 1;i>0;i--){
            for (scan = 0; scan < data.length - 1; scan++) {
                if (scan + i >= data.length)
                    continue;
                    if (data[scan].compareTo(data[scan + i]) > 0)
                        swap(data, scan, scan + i);
            }
        }
        for (int a = 0;a < data.length;a++) {
            System.out.print(data[a] + " ");
        }
    }
    private static <T extends Comparable<? super T>> void swap(T[] data, int i,int j) {
        T temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }

}
