package chapter15.PP151;


import chapter04.LinkedStack;
import chapter05.LinkedQueue;
import chapter06.ArrayUnorderedList;
import chapter06.UnorderedListADT;
import chapter15.TextCode.GraphADT;
import jsjf.QueueADT;
import jsjf.StackADT;

import java.util.*;

public class Graph<T> implements GraphADT<T> {
    protected int numVertices;    // number of vertices in the graph
    protected List<List<Integer>> adjMatrix;    // adjacency matrix
    protected List<T> vertices;    // values of vertices
    protected int modCount;

    public Graph()
    {
        vertices = new ArrayList<T>();
        adjMatrix = new ArrayList<List<Integer>>();
        modCount =0;
        numVertices = 0;
    }

    public String toString()
    {
        String result="顶点有：\n";
        for (int i = 0; i < numVertices; i++)
        {
            result += "" + i + "\t";
            result += vertices.get(i).toString() + "\n";
        }

        result +="\n"+"边有：\n";
        for (int i=0;i<numVertices;i++)
        {
            result += "" + vertices.get(i) + "的边有：";
            for (int j = 0; j < adjMatrix.get(i).size()-1; j++)
            {
                result += vertices.get(adjMatrix.get(i).get(j+1))+" ";
            }
            result += "\n";
        }

        return result;
    }
    @Override
    public void addVertex(T vertex) {
        vertices.add(vertex);
        List list = new ArrayList();
        list.add(numVertices);
        adjMatrix.add(list);//？？？
        numVertices++;
        modCount++;
    }

    @Override
    public void removeVertex(T vertex) {
        int index = getIndex(vertex);
        if (indexIsValid(index))
            vertices.remove(index);
        for (int i = 0;i < adjMatrix.get(index).size()-1;i++)
        {
            int x = adjMatrix.get(index).get(i+1);
            removeEdge(x,index);
        }
        adjMatrix.remove(index);
        numVertices--;
        modCount++;
    }

    @Override
    public void removeEdge(T vertex1, T vertex2) {
        int index1 = getIndex(vertex1);
        int index2 = getIndex(vertex2);
        if (indexIsValid(index1)&&indexIsValid(index2))
        {
            if (adjMatrix.get(index1).contains(index2))
            {
                (adjMatrix.get(index1)).remove(adjMatrix.get(index1).indexOf(index2));
                (adjMatrix.get(index2)).remove(adjMatrix.get(index2).indexOf(index1));
                modCount++;
            }
        }

    }
    public void removeEdge(int index1, int index2) {
        if (indexIsValid(index1)&&indexIsValid(index2))
        {
            if ((adjMatrix.get(index1)).contains(index2))
            {
                (adjMatrix.get(index1)).remove(adjMatrix.get(index1).indexOf(index2));
                (adjMatrix.get(index2)).remove(adjMatrix.get(index2).indexOf(index1));
                modCount++;
            }
        }

    }
    public void addEdge(int index1,int index2)
    {
        if (indexIsValid(index1)&&indexIsValid(index2))
        {
            (adjMatrix.get(index1)).add(index2);
            (adjMatrix.get(index2)).add(index1);

            modCount++;
        }
    }
    public void addEdge(T vertex1,T vertex2)
    {
        int index1 = getIndex(vertex1);
        int index2 = getIndex(vertex2);
        if (indexIsValid(index1)&&indexIsValid(index2))
        {
            (adjMatrix.get(index1)).add(index2);
            (adjMatrix.get(index2)).add(index1);

            modCount++;
        }
    }
    @Override
    public Iterator iteratorBFS(T startVertex) {
        int startIndex = getIndex(startVertex);
        Integer x;
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalQueue.enqueue(startIndex);
        visited[startIndex] = true;

        while (!traversalQueue.isEmpty())
        {
            x = traversalQueue.dequeue();
            resultList.addToRear(vertices.get(x));

            //Find all vertices adjacent to x that have not been visited
            //     and queue them up

            for (int i=1;i<adjMatrix.get(x).size();i++)
            {
                if (!visited[adjMatrix.get(x).get(i)])
                {
                    traversalQueue.enqueue((adjMatrix.get(x)).get(i));
                    visited[adjMatrix.get(x).get(i)] = true;
                }
            }
        }
        return new ListGraphIterator(resultList.iterator());
    }

    @Override
    public Iterator iteratorDFS(T startVertex) {
        int startIndex = getIndex(startVertex);
        Integer x;
        boolean found;
        StackADT<Integer> traversalStack = new LinkedStack<Integer>();
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        boolean[] visited = new boolean[numVertices];

        if (!indexIsValid(startIndex))
            return resultList.iterator();

        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalStack.push(startIndex);
        resultList.addToRear(vertices.get(startIndex));
        visited[startIndex] = true;

        while (!traversalStack.isEmpty())
        {
            x = traversalStack.peek();
            found = false;

            //Find a vertex adjacent to x that has not been visited
            //     and push it on the stack
            for (int i=1;i<adjMatrix.get(x).size()&&!found;i++)
            {
                if (!visited[adjMatrix.get(x).get(i)])
                {
                    traversalStack.push(adjMatrix.get(x).get(i));
                    resultList.addToRear(vertices.get(adjMatrix.get(x).get(i)));
                    visited[adjMatrix.get(x).get(i)] = true;
                    found = true;
                }
            }

            if (!found && !traversalStack.isEmpty())
                traversalStack.pop();
        }
        return new ListGraphIterator(resultList.iterator());
    }
    protected Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex)
    {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];
        QueueADT<Integer> traversalQueue = new LinkedQueue<Integer>();
        UnorderedListADT<Integer> resultList =
                new ArrayUnorderedList<Integer>();

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) ||
                (startIndex == targetIndex))
            return resultList.iterator();

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++)
            visited[i] = false;

        traversalQueue.enqueue(Integer.valueOf(startIndex));
        visited[startIndex] = true;
        pathLength[startIndex] = 0;
        predecessor[startIndex] = -1;

        while (!traversalQueue.isEmpty() && (index != targetIndex))
        {
            index = (traversalQueue.dequeue()).intValue();

            for (int i=1;i<adjMatrix.get(index).size();i++)
            {
                if (!visited[adjMatrix.get(index).get(i)])
                {
                    pathLength[adjMatrix.get(index).get(i)]=pathLength[index]+1;
                    predecessor[adjMatrix.get(index).get(i)] = index;
                    traversalQueue.enqueue((adjMatrix.get(index)).get(i));
                    visited[adjMatrix.get(index).get(i)] = true;
                }
            }
        }
        if (index != targetIndex)  // no path must have been found
            return resultList.iterator();

        StackADT<Integer> stack = new LinkedStack<Integer>();
        index = targetIndex;
        stack.push(Integer.valueOf(index));
        do
        {
            index = predecessor[index];
            stack.push(Integer.valueOf(index));
        } while (index != startIndex);

        while (!stack.isEmpty())
            resultList.addToRear(((Integer)stack.pop()));

        return new ListGraphIndexIterator(resultList.iterator());
    }

    public Iterator<T> iteratorShortestPath(int startIndex, int targetIndex)
    {
        UnorderedListADT<T> resultList = new ArrayUnorderedList<T>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return resultList.iterator();

        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);
        while (it.hasNext())
            resultList.addToRear(vertices.get(it.next()));
        return new ListGraphIterator(resultList.iterator());
    }
    @Override
    public Iterator iteratorShortestPath(T startVertex, T targetVertex) {
        return iteratorShortestPath(getIndex(startVertex),
                getIndex(targetVertex));
    }

    public int shortestPathLength(int startIndex, int targetIndex)
    {
        int result = 0;
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex))
            return 0;

        int index1;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex,
                targetIndex);

        if (it.hasNext())
            index1 = ((Integer)it.next()).intValue();
        else
            return 0;

        while (it.hasNext())
        {
            result++;
            it.next();
        }

        return result;
    }

    @Override
    public boolean isEmpty() {
        return numVertices==0;
    }

    @Override
    public boolean isConnected() {
        return false;
    }

    @Override
    public int size() {
        return numVertices;
    }

    public int getIndex(T vertex)
    {

        int index = numVertices-1;

        for (int i = 0; i < numVertices; i++) {
            if (vertex.equals(vertices.get(i))) {
                index = i;
                break;
            }
        }
        return index;
    }

    protected boolean indexIsValid(int index)
    {
        if (index<size())
            return true;
        else
            return false;
    }
    protected class ListGraphIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public ListGraphIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }


        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws  ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public T next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }

    /**
     * Inner class to represent an iterator over the indexes of this graph
     */
    protected class ListGraphIndexIterator implements Iterator<Integer>
    {
        private int expectedModCount;
        private Iterator<Integer> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a graph traversal
         */
        public ListGraphIndexIterator(Iterator<Integer> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return  true if this iterator has at least one more element to deliver
         *          in the iteration
         * @throws  ConcurrentModificationException if the collection has changed
         *          while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public Integer next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported.
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove()
        {
            throw new UnsupportedOperationException();
        }
    }
}

