package chapter15.TextCode;

/**
 * @author LbZhang
 * @version 创建时间：2015年12月3日 上午10:22:07
 * @description 加权图的接口设计
 */
public interface NetworkADT<T> extends GraphADT<T> {

	public void addEdge(T vertex1, T vertex2, double weight);

	
	public double shortestPathWeight(T vertex1, T vertex2);

}
