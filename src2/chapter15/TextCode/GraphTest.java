package chapter15.TextCode;

public class GraphTest {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addVertex(1);
        graph.addVertex(2);
        graph.addVertex(3);

        graph.addEdge(1,2);
        graph.addEdge(1,3);
        graph.addEdge(2,1);
        graph.addEdge(2,3);
        graph.addEdge(3,1);
        graph.addEdge(3,2);
        System.out.println(graph);
        System.out.println(graph.isConnected());
        graph.removeEdge(1,3);
        System.out.println(graph.isConnected());
        System.out.println(graph);
        System.out.println(graph.iteratorBFS(1));
    }
}
