package chapter15.PP157;

import java.util.Scanner;

public class PP157Test {
    public static void main(String[] args) {
        FlightNetWork<String> flightNetWork = new FlightNetWork<>();
        flightNetWork.addCity("A");
        flightNetWork.addCity("B");
        flightNetWork.addCity("V");
        flightNetWork.addCity("C");
        flightNetWork.addCity("E");

        flightNetWork.addEdge(0,1,100);
        flightNetWork.addEdge(0,2,300);
        flightNetWork.addEdge(0,2,50);
        flightNetWork.addEdge(1,3,200);
        flightNetWork.addEdge(1,3,150);
        flightNetWork.addEdge(2,4,100);
        flightNetWork.addEdge(3,4,50);
        flightNetWork.addEdge(0,4,500);

        System.out.println("现有城市A、B、V、C、E，请输入其中两个城市以获取具体信息:");
        Scanner scan = new Scanner(System.in);
        String a  = scan.nextLine();
        String b = scan.nextLine();

        System.out.println(flightNetWork);
        flightNetWork.getShortestWeightPath(a,b);
        flightNetWork.getShortestPath(a,b);
    }
}
