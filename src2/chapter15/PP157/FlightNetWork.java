package chapter15.PP157;

import java.util.Iterator;

public class FlightNetWork<T> extends UndirectedGraph<T>
{
    public FlightNetWork()
    {
        super();
    }

    public void addCity(T city)
    {
        addVertex(city);
    }

    public void addEdge(T city1,T city2,double weight)
    {
        addEdge(getIndex(city1),getIndex(city2),weight);
    }

    public void getShortestPath(T city1,T city2)
    {
        int index1 = getIndex(city1);
        int index2 = getIndex(city2);

        if (shortestPathLength(index1,index2)==0)
            System.out.println("无法连通城市:"+city1+" 和 "+city2);
        else {
            System.out.println("最短路径长度为："+shortestPathLength(index1,index2));
            String result = "";
            Iterator iterator = iteratorShortestPath(index1,index2);
            while (iterator.hasNext())
                result += iterator.next()+" --> ";
            System.out.println("最短路径为："+result+"null");
        }
    }
    public void getShortestWeightPath(T city1,T city2)
    {
        if (shortestPathLength(city1,city2)<Double.POSITIVE_INFINITY)
            System.out.println("最便宜的价格为："+shortestPathWeight(city1,city2));
        else
            System.out.println("无法连通城市:"+city1+" 和 "+city2);
    }
}
