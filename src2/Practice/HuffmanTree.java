package Practice;

import chapter06.ArrayUnorderedList;
import chapter06.UnorderedListADT;
import chapter12.ArrayHeap;

import java.util.ArrayList;
import java.util.Stack;

public class HuffmanTree
{
    private HuffmanTreeNode mRoot;	// 根结点
    private String[] codes = new String[26];

    public HuffmanTree(HuffmanTreeNode[] array) {
        HuffmanTreeNode parent = null;
        ArrayHeap<HuffmanTreeNode> heap = new ArrayHeap();
        for (int i=0;i<array.length;i++)
        {
            heap.addElement(array[i]);
        }

        for(int i=0; i<array.length-1; i++) {
            HuffmanTreeNode left = heap.removeMin();  // 最小节点是左孩子
            HuffmanTreeNode right = heap.removeMin();


            parent = new HuffmanTreeNode(null,left,right,left.getWeight()+right.getWeight(),' ');
            left.setParent(parent);
            right.setParent(parent);

            heap.addElement(parent);
        }

        mRoot = parent;
    }
    public String toString()
    {
        UnorderedListADT<HuffmanTreeNode> nodes =
                new ArrayUnorderedList();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        HuffmanTreeNode current;
        String result = "";
        int printDepth = this.getHeight()-1;
        int possibleNodes = (int)Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(mRoot);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes)
        {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel)
            {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            }
            else
            {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)) ; i++)
                {
                    result = result + " ";
                }
            }
            if (current != null)
            {
                result = result + current.getWeight();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            }
            else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }
        }
        return result;
    }
    public int getHeight()
    {
        return height(mRoot);
    }


    private int height(HuffmanTreeNode node)
    {
        if(node==null){
            return 0;
        }
        else {
            int leftTreeHeight = height(node.getLeft());
            int rightTreeHeight= height(node.getRight());
            return leftTreeHeight>rightTreeHeight ? (leftTreeHeight+1):(rightTreeHeight+1);
        }
    }


    protected void inOrder( HuffmanTreeNode node,
                            ArrayList<HuffmanTreeNode> tempList)
    {
        if (node != null)
        {
            inOrder(node.getLeft(), tempList);
            if (node.getCharacter()!=' ')
                tempList.add(node);

            inOrder(node.getRight(), tempList);
        }
    }

    public String[] getEncoding() {
        ArrayList<HuffmanTreeNode> arrayList = new ArrayList();
        inOrder(mRoot,arrayList);
        for (int i=0;i<arrayList.size();i++)
        {
            HuffmanTreeNode node = arrayList.get(i);
            String result ="";
            int x = node.getCharacter()-'a';
            Stack stack = new Stack();
            while (node!=mRoot)
            {
                if (node==node.getParent().getLeft())
                    stack.push(0);
                if (node==node.getParent().getRight())
                    stack.push(1);

                node=node.getParent();
            }
            while (!stack.isEmpty())
            {
                result +=stack.pop();
            }
            codes[x] = result;
        }
        return codes;
    }

    public HuffmanTreeNode getmRoot() {
        return mRoot;
    }
}
