package Practice;

public class HuffmanTreeNode implements Comparable<HuffmanTreeNode> {
    private int weight;
    private HuffmanTreeNode parent, left, right;
    private char character;

    public HuffmanTreeNode(HuffmanTreeNode parent, HuffmanTreeNode left, HuffmanTreeNode right, int weight, char character) {
        this.weight = weight;
        this.character = character;
        this.parent = parent;
        this.left = left;
        this.right = right;
    }

    @Override
    public int compareTo(HuffmanTreeNode huffmanTreeNode) {
        return this.weight - huffmanTreeNode.getWeight();
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public HuffmanTreeNode getParent() {
        return parent;
    }

    public void setParent(HuffmanTreeNode parent) {
        this.parent = parent;
    }

    public HuffmanTreeNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanTreeNode left) {
        this.left = left;
    }

    public HuffmanTreeNode getRight() {
        return right;
    }

    public void setRight(HuffmanTreeNode right) {
        this.right = right;
    }

    public char getCharacter() {
        return character;
    }

    public void setElement(char character) {
        this.character = character;
    }
}