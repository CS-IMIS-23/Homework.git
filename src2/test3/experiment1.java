package test3;

import chapter10.TextCode.BinaryTreeNode;
import chapter10.TextCode.LinkedBinaryTree;

import java.util.Iterator;

public class experiment1 {
    public static void main(String[] args) {
        BinaryTreeNode btn1 = new BinaryTreeNode("A");
        BinaryTreeNode btn2 = new BinaryTreeNode("B");
        BinaryTreeNode btn3 = new BinaryTreeNode("C");
        BinaryTreeNode btn4 = new BinaryTreeNode("D");
        BinaryTreeNode btn5 = new BinaryTreeNode("E");

        LinkedBinaryTree lbt1 = new LinkedBinaryTree(btn4.getElement());
        LinkedBinaryTree lbt2 = new LinkedBinaryTree(btn5.getElement(),lbt1,lbt1);
        LinkedBinaryTree lbt3 = new LinkedBinaryTree(btn3.getElement(),lbt1,lbt2);
        LinkedBinaryTree lbt4 = new LinkedBinaryTree(btn2.getElement(),lbt1,lbt2);
        LinkedBinaryTree lbt5 = new LinkedBinaryTree(btn1.getElement(),lbt4,lbt3);

        System.out.println("输出树："+lbt5);
        System.out.println("lbt5的右孩子为："+lbt5.getRight());

        Iterator iterator1 = lbt5.iteratorPostOrder();
        Iterator iterator2 = lbt5.iteratorPreOrder();
        System.out.println("后序遍历");
        while(iterator1.hasNext())
            System.out.print(iterator1.next()+" ");
        System.out.println();
        System.out.println("前序遍历");
        while(iterator2.hasNext())
            System.out.print(iterator2.next()+" ");

    }
}
