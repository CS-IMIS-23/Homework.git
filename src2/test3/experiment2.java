package test3;
import chapter10.TextCode.LinkedBinaryTree;

public class experiment2 {
    public static void main(String[] args) {

        String[] array1 = new String[]{"H","D","I","B","E","M","J","N","A","F","C","K","G","L"};//中序序列
        String[] array2 = new String[]{"A","B","D","H","I","E","J","M","N","C","F","G","K","L"};//先序序列
        LinkedBinaryTree linkedBinaryTree = new LinkedBinaryTree();
        linkedBinaryTree.buildTree(array1,array2);
        System.out.println(linkedBinaryTree);
    }
}
