package test3.experiment4;

public class Node {

    private  String data;//数据
    private Node lchild;// 左子树
    private Node rchild;// 右子树

  Node() {}
  Node(String data) {
      this.data = data;
  }
  Node(String data, Node lchild, Node rchild) {
      super();
      this.data = data;
      this.lchild = lchild;
      this.rchild = rchild;
  }
   public String getData() {
      return data;
  }
   public Node getLchild() {
       return lchild;
             }
   public Node getRchild() {
                 return rchild;
}

}
