package test3.experiment4;

import java.util.ArrayList;

/**
 * 表达式二叉树类
 * @author yuxiu
 *
 */
public class Formaluetree {
    private String s = "";
    private Node root;     //根节点

    /**
     * 创建表达式二叉树
     *
     * @param str 表达式
     */
    public void creatTree(String str) {
        int count = 0;//记录表达式中操作符数
        int time = 1;//记录+ - 个数
        //声明一个数组列表，存放的操作符，加减乘除
        ArrayList<String> operList = new ArrayList<String>();
        //声明一个数组列表，存放节点的数据

        ArrayList<Node> numList = new ArrayList<Node>();
        //第一，辨析出操作符与数据，存放在相应的列表中
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);          //取出字符串的各个字符
            if (ch >= '0' && ch <= '9') {
                s += ch;
            }
            else {
                numList.add(new Node(s));
                s = "";
                operList.add(ch+"");
                count++;
            }
        }
        //把最后的数字加入到数字节点中
        numList.add(new Node(s));
        int max = 0;//记录操作符中乘除运算符的最大索引值
        for(int a = 0;a < operList.size();a++){
            if (operList.get(a).equals("*") ||operList.get(a).equals("/"))
                max = a;

        }


        while (operList.size() > 0) {    //第三步，重复第二步，直到操作符取完为止
            //第二，取出前两个数字和一个操作符，组成一个新的数字节点
            for (int a = 0; a < operList.size(); a++){
               if(operList.get(a).equals("*") || operList.get(a).equals("/")){
                   Node left = numList.remove(a);
                   Node right = numList.remove(a);
                   String oper = operList.remove(a);
                   Node node = new Node(oper, left, right);
                   numList.add(a, node);
                   a--;
               }
               else
                   time++;

            }
            Node left = numList.remove(0);
            Node right = numList.remove(0);
            String oper = operList.remove(0);
            Node node = new Node(oper, left, right);

            numList.add(0, node);       //将新生的节点作为第一个节点，同时以前index=0的节点变为index=1
        }
        //第四步，让根节点等于最后一个节点
        root = numList.get(0);
    }

    /**
     * 输出结点数据
     */
    public String output() {
        String x = output(root);      //从根节点开始遍历
        return x;

    }

    /**
     * 输出结点数据
     *
     * @param node
     */
    public String output(Node node) {
        String result = "";
        if (node.getLchild() != null) {       //如果是叶子节点就会终止
            output(node.getLchild());
            System.out.print(" ");
            result = result+node.getLchild()+" ";
        }
        if (node.getRchild() != null) {
            output(node.getRchild());
            System.out.print(" ");
            result = result+node.getRchild()+" ";
        }
        System.out.print(node.getData());     //遍历包括先序遍历（根左右）、中序遍历（左根右）、后序遍历（左右根）
        result = result+node.getData()+" ";

        return result;
    }

}

