package test3.experiment4;

import chapter10.TextCode.ExpressionTree;
import chapter10.TextCode.PostfixEvaluator;

import java.util.Scanner;

public class experiment4Test {
    public static void main(String[] args) {
        Formaluetree tree = new Formaluetree();
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入中缀表达式：");
        String string = scan.nextLine();
        tree.creatTree(string);//创建表达式的二叉树
         //tree.creatTree("4+2*5-6/3");
        System.out.println("后缀表达式：");
        String a = tree.output();//输出验证
        //System.out.println(a);
        PostfixEvaluator evaluator = new PostfixEvaluator();
        int result =  evaluator.evaluate(a);
        System.out.println("计算结果为："+result);


    }

}

