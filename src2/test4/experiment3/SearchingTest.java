package test4.experiment3;

import chapter10.TextCode.BinaryTreeNode;
import chapter10.TextCode.LinkedBinaryTree;

public class SearchingTest {
    public static void main(String[] args) {
        int[] data = {34,23,45,1,32,2308,5,21,15};
        Integer[] data1 = {1,5,15,21,23,32,34,45,2308};
        int[] data5 = {1,5,15,21,23,32,34,45,2308};
        Integer[] data2 = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
        Integer[] data4 = {34,23,45,1,32,2308,5,21,15};

        System.out.println("顺序查找结果："+Searching.linearSearch(data4,0,8,1));//顺序查找
        System.out.println("二分查找到的索引值："+Searching.binarySearch(data5,0,8,21));//二分查找
        System.out.println("插值查找结果："+Searching.InsertionSearch(data1,0,8,1));//插值查找

        Searching searching = new Searching();
        System.out.println("斐波那契查找结果："+searching.FibonacciSearch(data2,9));//斐波那契查找


        //System.out.println("二叉树查找结果："+searching.BinarySearchTree(1));//二叉树查找


        System.out.println("分块查找结果："+searching.blockSearch(data,data,9,4));//分块查找

        int[] data3 = {34,23,45,1,32,2308,5,21,15};
        System.out.println(searching.HashSearch(data3,1));//哈希查找

    }
}
