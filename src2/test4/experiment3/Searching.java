package test4.experiment3;

import chapter10.TextCode.BinaryTreeNode;
import chapter11.TextCode.exceptions.ElementNotFoundException;

import java.util.ArrayList;

public class Searching {
    /**
     * 线性查找 在没有找到之前 需要一直遍历
     */
    public static <T extends Comparable<T>> boolean linearSearch(T[] data, int min, int max, T target) {

        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }
        return found;
    }
    /**
     * 二分查找：二分查找需要实现数组列表有序，然后每次考察中间元素，排除一半，最好的方法是使用递归实现
     */
    public static  int binarySearch(int[] data, int min, int max, int target) {
        //boolean flag = false;
        int mid = (max + min) / 2;

        if (data[mid] == (target)) {
            //flag = true;
            return mid;
        }
        if (data[mid] > (target)) {//中间大于目标
            if (min <= mid - 1) {
                return binarySearch(data, min, mid - 1, target);
            }
        }
        if (data[mid] < (target)) {
            if (mid + 1 <= max) {
                return binarySearch(data, mid + 1, max, target);
            }
        }
        return mid;
    }
    public static int binarySearch(int a[], int value){
        int low, high, mid;
        int n = a.length;
        low = 0;
        high = n-1;
        while(low <= high)
        {
            mid = (low + high)/2;
            if(a[mid] == value)
                return mid;
            if(a[mid] > value)
                high = mid-1;
            if(a[mid] < value)
                low = mid + 1;
        }
        return -1;

    }
    /**
     * 插值查找：基于二分查找算法，将查找点的选择改进为自适应选择，可以提高查找效率
     */
    public static <T extends Comparable<? super T>> boolean InsertionSearch(Integer[] data, int min, int max, int target){
        boolean flag= false;
        int mid = min+(target-data[min])/(data[max]-data[min])*(max-min);

        if (mid <= max || mid >= min) {
            if (data[mid] == (target)) {
                flag = true;
            } else if (data[mid] > (target)) {//中间大于目标
                if (min <= mid - 1) {
                    flag = InsertionSearch(data, min, mid - 1, target);
                }
            } else if (data[mid] < target) {
                if (mid + 1 <= max) {
                    flag = InsertionSearch(data, mid + 1, max, target);
                }
            }
        }
        return flag;
    }
    /**
     * 斐波那契查找：二分查找的一种提升算法，通过运用黄金比例的概念在数列中选择查找点进行查找，提高查找效率
     */
    /*构造一个斐波那契数组*/
    int max_size=20;//斐波那契数组的长度
    public void Fibonacci(int[] F)
    {
        F[0]=0;
        F[1]=1;
        for(int i=2;i<max_size;++i)
            F[i]=F[i-1]+F[i-2];
    }

    public <T extends Comparable<? super T>> boolean FibonacciSearch(Integer[] data, int target){

        int low = 0;
        int high = data.length;
        int[] F = new int[max_size];
        Fibonacci(F);//构造一个斐波那契数组F

        int k=0;
        while(high > F[k]-1)//计算max(数组长度)位于斐波那契数列的位置
            k++;

        int[] temp = new int[F[k]-1];//将数组data扩展到F[k]-1的长度
        for (int i = 0;i < high;i++){
            temp[i] = data[i];
        }

        for(int i = high;i < F[k]-1;i++)
            temp[i] = data[high-1];


        while(low <= high)
        {
            int mid = low+F[k-1]-1;
            if(target < temp[mid])
            {
                high = mid-1;
                k--;
            }
            else if(target > temp[mid])
            {
                low = mid + 1;
                k--;
                k--;
            }
            else
            {
                if(mid < high)
                    return true; //若相等则说明mid即为查找到的位置
                else
                    return false; //若mid>=n则说明是扩展的数值,返回n-1
            }
        }
        return false;
    }
    /**
     * 二叉树查找算法：二叉查找树是先对待查找的数据进行生成树，
     * 确保树的左分支的值小于右分支的值，
     * 然后在就行和每个节点的父节点比较大小
     */
    protected BinaryTreeNode root;
    private BinaryTreeNode findNode(int targetElement,
                                       BinaryTreeNode next) {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode temp = findNode(targetElement, next.getLeft());

        if (temp == null)
            temp = findNode(targetElement, next.getRight());

        return temp;
    }
    public int find(int targetElement) throws ElementNotFoundException {
        BinaryTreeNode current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        return (int) current.getElement();
    }
    public int BinarySearchTree(int targetElement) {

        return find(targetElement);
    }
    /**
     * 分块查找：将n个数据元素"按块有序"划分为m块（m ≤ n）。每一块中的结点不必有序，但块与块之间必须"按块有序"
     * @param index
     * 索引表，其中放的是各块的最大值
     * @param st
     *顺序表，
     * @param key
     * 要查找的值
     * @param m
     * 顺序表中各块的长度相等，为m
     * @return
     */
    public static int blockSearch(int[] index, int[] st, int key, int m) {
        // 在序列st数组中，用分块查找方法查找关键字为key的记录
        // 1.在index[ ] 中折半查找，确定要查找的key属于哪个块中
        int i = binarySearch(index, key);
        if (i >= 0) {
            int j = i > 0 ? i * m : i;
            int len = (i + 1) * m;
            // 在确定的块中用顺序查找方法查找key
            for (int k = j; k < len; k++) {
                if (key == st[k]) {
                    System.out.println("查询成功");
                    return k;
                }
            }
        }
        System.out.println("查找失败");
        return -1;
    }

    /**
     * 哈希查找：
     */
    private static class Node {// 哈希结点
        int key; // 链表中的键
        Node next; // 下一个同义词
    }
    /* 在哈希表中查找关键字key */
    public boolean HashSearch(int[] data, int key) {
        int p = 1;
        // 寻找小于或等于最接近表长的素数
        for (int i = data.length; i > 1; i--) {
            if (isPrimes(i)) {
                p = i;
                break;
            }
        }
        // 构建哈希表
        Node[] hashtable = createHashTable(data, p);
        // 查找key是否在哈希表中
        int k = key % p;
        Node cur = hashtable[k];
        while (cur != null && cur.key != key) {
            cur = cur.next;
        }
        if (cur == null) {
            return false;
        } else {
            return true;
        }

    }
    /*用求余，链表法构建哈希表*/
    public Node[] createHashTable(int[] data, int p) {
        Node[] hashtable = new Node[p];
        int k;        //哈希函数计算的单元地址
        for (int i = 0; i < data.length; i++) {
            Node node = new Node();
            node.key = data[i];
            k = data[i] % p;
            if (hashtable[k] == null) {
                hashtable[k] = node;
            }else {
                Node current = hashtable[k];
                while(current.next != null) {
                    current = current.next;
                }
                current.next = node;
            }
        }
        return hashtable;
    }

    public boolean isPrimes(int n) {
        for(int i = 2; i <= Math.sqrt(n); i++ ) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}