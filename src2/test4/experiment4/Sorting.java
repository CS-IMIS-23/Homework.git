package test4.experiment4;

import chapter10.TextCode.BinaryTreeNode;
import chapter10.TextCode.LinkedBinaryTree;
import chapter11.TextCode.LinkedBinarySearchTree;
import chapter12.Practice.ArrayHeap;

import java.util.Iterator;

public class Sorting {
    /**
     *堆排序
     */
    public String HeapSort(Object[] data) {
        ArrayHeap temp = new ArrayHeap();

        // copy the array into a heap
        for (int i = 0; i < data.length; i++)
            temp.addElement(data[i]);
        // place the sorted elements back into the array
        int count = 0;
        while (!(temp.isEmpty())) {
            data[count] = temp.removeMax();
            count++;
        }
        String result = "";
        for (int i = 0; i < data.length; i++) {
            result += data[i] + " ";
        }
        return result;
    }
    /**
     *希尔排序
     */
    public  String shellSort(int[] array) {
        int len = array.length;
        int temp, gap = len / 2;
        while (gap > 0) {
            for (int i = gap; i < len; i++) {
                temp = array[i];
                int preIndex = i - gap;
                while (preIndex >= 0 && array[preIndex] > temp) {
                    array[preIndex + gap] = array[preIndex];
                    preIndex -= gap;
                }
                array[preIndex + gap] = temp;
            }
            gap /= 2;
        }
        String result = "";
        for (int i = 0; i < array.length; i++) {
            result += array[i] + " ";
        }
        return result;

    }
    /**
     *二叉树排序
     */
    public String binaryTreeSorting(LinkedBinarySearchTree lbst){
        Iterator iterator = lbst.iteratorInOrder();

        String result = "";
        while(iterator.hasNext())
            result += iterator.next()+" ";
        return result;
    }
    /**
     * 归并排序：
     */
    public  <T extends Comparable<? super T>> String mergeSort(T[] data,
                                                                   int min, int max) {
        if (min < max) {
            int mid = (min + max) / 2;
            mergeSort(data, min, mid);
            mergeSort(data, mid + 1, max);
            merge(data, min, mid, max);

        }
        return merge(data, min, (min + max) / 2, max);

    }
    /**
     * Merges two sorted subarrays of the specified array.
     */
    private  <T extends Comparable<? super T>> String merge(T[] data,
                                                                int first, int mid, int last) {

        T[] temp = (T[]) (new Comparable[data.length]);

        int first1 = first, last1 = mid; // endpoints of first subarray
        int first2 = mid + 1, last2 = last; // endpoints of second subarray

        //记录下标
        int index = first1; // next index open in temp array
        // Copy smaller item from each subarray into temp until one
        // of the subarrays is exhausted
        while (first1 <= last1 && first2 <= last2) {
            if (data[first1].compareTo(data[first2]) < 0) {
                temp[index] = data[first1];
                first1++;
            } else {
                temp[index] = data[first2];
                first2++;
            }
            index++;
        }
        // Copy remaining elements from first subarray, if any
        while (first1 <= last1) {
            temp[index] = data[first1];
            first1++;
            index++;
        }
        // Copy remaining elements from second subarray, if any
        while (first2 <= last2) {
            temp[index] = data[first2];
            first2++;
            index++;
        }
        // Copy merged data into original array
        for (index = first; index <= last; index++)
            data[index] = temp[index];

        String result = "";
        for (int i = 0; i < data.length; i++) {
            result += data[i] + " ";
        }
        return result;

    }
    public static void main(String[] args) {

        Integer[] data = {36, 30, 18, 40, 32, 45, 22, 50};
        Sorting hs = new Sorting();
        hs.HeapSort(data);
        System.out.println();

        int[] data2 = {36, 30, 18, 40, 32, 45, 22, 50};
        hs.shellSort(data2);
        System.out.println("希尔排序结果：");
        for (int i = 0; i < data2.length; i++) {
            System.out.print(data2[i] + " ");
        }
        System.out.println();

        LinkedBinarySearchTree linkedBinarySearchTree = new LinkedBinarySearchTree();
        linkedBinarySearchTree.addElement(4);
        linkedBinarySearchTree.addElement(2);
        linkedBinarySearchTree.addElement(11);
        linkedBinarySearchTree.addElement(9);
        linkedBinarySearchTree.addElement(5);

        System.out.println("二叉树排序结果：");
        hs.binaryTreeSorting(linkedBinarySearchTree);
        System.out.println();

        hs.mergeSort(data,0,7);
        System.out.println("归并排序结果：");
        for (int i = 0; i < data2.length; i++) {
            System.out.print(data2[i] + " ");
        }
        System.out.println();

    }
}