package test4.experiment2.test;

import test4.experiment2.cn.edu.besti.cs1723.ZYJ2308.Sorting;

public class SortingTest {
    public static void main(String[] args) {
        Integer[] a = {3, 2, 6, 4, 5, 1, 7, 8, 9, 2308};
        Integer[] b = {9, 8, 7, 6, 2308, 5, 4, 3, 2, 1};
        Integer[] c = {3, 2, 6, 4, 5, 1, 2308, 7, 8, 9};

        System.out.println(Sorting.selectionSort(a));
        System.out.println(Sorting.selectionSort(b));
        System.out.println(Sorting.selectionSort(c));

    }
}
