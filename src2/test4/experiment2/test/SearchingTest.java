package test4.experiment2.test;

import test4.experiment2.cn.edu.besti.cs1723.ZYJ2308.Searching;

public class SearchingTest {
    public static void main(String[] args) {
        Integer[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        Integer[] b = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};//逆序
        System.out.println(Searching.linearSearch(a,0,9,4));
        System.out.println(Searching.linearSearch(b,4,9,2));
        System.out.println(Searching.linearSearch(a,4,9,10));
        System.out.println(Searching.linearSearch(b,4,9,1));
        System.out.println(Searching.linearSearch(b,0,4,1));
        System.out.println(Searching.linearSearch(a,0,4,7));

    }


}
