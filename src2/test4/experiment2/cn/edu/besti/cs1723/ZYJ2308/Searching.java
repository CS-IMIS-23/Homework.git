package test4.experiment2.cn.edu.besti.cs1723.ZYJ2308;

public class Searching {
    /**
     * 线性查找 在没有找到之前 需要一直遍历
     */
    public static <T extends Comparable<T>> boolean linearSearch(T[] data,
                                                                 int min, int max, T target) {

        int index = min;
        boolean found = false;

        while (!found && index <= max) {
            found = data[index].equals(target);
            index++;
        }

        return found;

    }
}
