package test4.experiment2.cn.edu.besti.cs1723.ZYJ2308;

public class Sorting {
    /**
     * 选择排序：
     */
    public static String result2 = "";
    public static <T extends Comparable<? super T>> String selectionSort(T[] data) {
        int min;
        T temp;
        for (int i = 0; i < data.length - 1; i++) {
            min = i;
            // /找出最小的下标
            for (int j = i + 1; j < data.length; j++) {
                if (data[min].compareTo(data[j]) > 0) {
                    min = j;
                }
            }
            // 交换位置
            swap(data, min, i);
        }
        String result = "";
        for (int a = 0;a < data.length;a++){
            result += data[a]+" ";
        }

        for (int a = data.length-1;a >= 0 ;a--) {
            result2 += data[a] + " ";
        }
        return result;

    }
    private static <T extends Comparable<? super T>> void swap(T[] data, int i,
                                                               int j) {
        T temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }
}
