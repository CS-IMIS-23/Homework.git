package chapter10.TextCode;



/**
 * BinaryTreeNode 二叉树中有左子节点和右子节点的节点
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class BinaryTreeNode<T>
{
    protected T element;
    protected BinaryTreeNode<T> left, right;

    /**
     * 使用指定的数据创建一个新的树节点
     * @param obj 将成为新树节点的一部分的元素
    */
    public BinaryTreeNode(T obj) 
    {
        element = obj;
        left = null;
        right = null;
    }

    /**
     * 使用指定的数据创建一个新的树节点
     * @param obj the element that will become a part of the new tree node
     * @param left 这个树是这个节点的左子树//-----------为什么是LinkedBinaryTree型的？？？？
     * @param right 这个树是这个节点的右子树
     */
    public BinaryTreeNode(T obj, LinkedBinaryTree<T> left, LinkedBinaryTree<T> right) 
    {
        element = obj;
        if (left == null)
            this.left = null;
        else
            this.left = left.getRootNode();
        
         if (right == null)
            this.right = null;
        else
            this.right = right.getRootNode();
    }
    public boolean isleaf(){
        if (this.left==null && this.right==null)
            return true;
        else
            return false;
    }
    public int count(){
        int result = 1;

        if(left != null)
            result += left.count();

        if(right != null)
            result += right.count();

        return result;
    }
    public String toString() {
        return element.toString();
    }

    /**
     * 返回该节点的非空子节点数
     * @return the integer number of non-null children of this node 
     */
    public int numChildren() 
    {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }
    
    /**
     * 返回节点处的元素
     * @return the element stored at this node
     */
    public T getElement() 
    {
        return element;
    }
    
    /**
     * 返回该节点的右子节点
     * @return the right child of this node
     */
    public BinaryTreeNode<T> getRight() 
    {
        return right;
    }
    
    /**
     * 设置此节点的右子节点
     * @param node the right child of this node
     */
    public void setRight(BinaryTreeNode<T> node) 
    {
        right = node;
    }
    
    /**
     * Return the left child of this node.
     * @return the left child of the node
     */
    public BinaryTreeNode<T> getLeft() 
    {
        return left;
    }
    
    /**
     * Sets the left child of this node.
     * @param node the left child of this node
     */
    public void setLeft(BinaryTreeNode<T> node) 
    {
        left = node;
    }

//    public void inorder(ArrayIterator<T> iter){
//        if(left != null)
//            left.inorder(iter);
//
//        iter.add(element);
//
//        if(right != null)
//            right.inorder(iter);
//    }
//
    /*
      Performs an preorder traversal on this subtree,updating the specified iterator.
     */
//    public void preorder(ArrayIterator<T> iter){
//        iter.add(element);
//
//        if(left != null)
//            left.preorder(iter);
//
//        if(right != null)
//            right.preorder(iter);
//    }
//
//    /*
//      Performs an postorder traversal on this subtree,updating the specified iterator.
//     */
//    public void postorder(ArrayIterator<T> iter){
//        if(left != null)
//            left.postorder(iter);
//
//        if(right != null)
//            right.postorder(iter);
//
//        iter.add(element);
//    }

}

