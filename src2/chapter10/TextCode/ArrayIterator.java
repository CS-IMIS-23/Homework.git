package chapter10.TextCode;

import chapter06.ArrayList;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

public class ArrayIterator<T> extends ArrayList<T> implements Iterator<T> {

    int iteratorModCount;
    int current;

    /**
     * Sets up this iterator using the specified modCount.
     */
    public ArrayIterator()
    {
        iteratorModCount = modCount;
        current = 0;
    }

    /**
     * Returns true if this iterator has at least one more element
     * to deliver in the iteration.
     *
     * @return  true if this iterator has at least one more element to deliver
     *          in the iteration
     * @throws  ConcurrentModificationException if the collection has changed
     *          while the iterator is in use
     */

    public boolean hasNext() throws ConcurrentModificationException
    {
        return super.iterator().hasNext();
    }

    /**
     * Returns the next element in the iteration. If there are no
     * more elements in this iteration, a NoSuchElementException is
     * thrown.
     *
     * @return  the next element in the iteration
     * eption if an element not found exception occurs
     * @throws  ConcurrentModificationException if the collection has changed
     */
    public T next() throws ConcurrentModificationException
    {
        return super.iterator().next();
    }


    /**
     * The remove operation is not supported in this collection.
     *
     * @throws UnsupportedOperationException if the remove method is called
     */
    public void remove() throws UnsupportedOperationException
    {
        throw new UnsupportedOperationException();
    }
}
