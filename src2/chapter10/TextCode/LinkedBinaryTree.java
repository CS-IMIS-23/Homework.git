package chapter10.TextCode;

import java.util.*;

import chapter06.ArrayUnorderedList;
import chapter06.UnorderedListADT;
import chapter10.TextCode.exceptions.ElementNotFoundException;
import jsjf.EmptyCollectionException;

public class LinkedBinaryTree<T> implements BinaryTreeADT<T>, Iterable<T> {
    protected BinaryTreeNode<T> root;
    protected int modCount;

    /**
     * 创建一个空的二叉树.
     */
    public LinkedBinaryTree() {
        root = null;
    }

    /**
     * 创建以指定元素为根的二叉树
     *
     * @param element the element that will become the root of the binary tree
     */
    public LinkedBinaryTree(T element) {
        root = new BinaryTreeNode<T>(element);
    }

    /**
     * 创建一个二叉树，指定的元素作为其根，给定的树作为其左子树和右子树
     *
     * @param element the element that will become the root of the binary tree
     * @param left    the left subtree of this tree
     * @param right   the right subtree of this tree
     */
    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right) {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);//？？？
        root.setRight(right.root);
    }

    /**
     * 返回根节点
     *
     * @return 二叉树的根节点
     */
    public BinaryTreeNode getRoot() {
        return root;
    }

    /**
     * 设置二叉树的根节点
     *
     * @param root 设置后的根节点
     */
    public void setRoot(BinaryTreeNode root) {
        this.root = root;
    }


    public BinaryTreeADT removeRightSubtree() {
        if (getRoot() == null)
            throw new IllegalArgumentException("tree is empty");

        // detach right subtree and save in rightSubtree
        LinkedBinaryTree rightSubtree = new LinkedBinaryTree();
        rightSubtree.setRoot(getRoot().getRight());
        getRoot().setRight(null);

        return (BinaryTreeADT) rightSubtree;
    }

    public void removeAllElements() {
        BinaryTreeNode<T> root = null;
        this.root = root;
    }

    //计算树中叶结点的个数,
    public int num = 0;

    public int LeafNumber(BinaryTreeNode node) {
        if (node != null) {
            if (node.left == null && node.right == null) {
                return 1;
            }
            return LeafNumber(node.left) + LeafNumber(node.right);
        }
        return 0;
    }

    public int CountLeaf() {
        return LeafNumber(root);
    }

    /**
     * 返回对根元素的引用
     *
     * @return a reference to the specified target
     * @throws EmptyCollectionException if the tree is empty
     */
    public T getRootElement() throws EmptyCollectionException {
        return root.element;
    }

    /**
     * 返回对根节点的引用
     *
     * @return a reference to the specified node
     * @throws EmptyCollectionException if the tree is empty
     */
    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException {
        return root;
    }

    /**
     * Returns the left subtree of the root of this tree.
     *
     * @return a link to the left subtree fo the tree
     */
    public LinkedBinaryTree<T> getLeft() {
        if (root == null) {
            throw new EmptyCollectionException("Get left operation failed. the tree is empty.");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getLeft();
        return result;
    }

    /**
     * Returns the right subtree of the root of this tree.
     *
     * @return a link to the right subtree of the tree
     */
    public LinkedBinaryTree<T> getRight() {
        if (root == null) {
            throw new EmptyCollectionException("Get left operation failed. the tree is empty.");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getRight();
        return result;
    }

    /**
     * Returns true if this binary tree is empty and false otherwise.
     *
     * @return true if this binary tree is empty, false otherwise
     */
    public boolean isEmpty() {
        return (root == null);
    }

    /**
     * Returns the integer size of this tree.
     *
     * @return the integer size of the tree
     */
    public int size() {
        int result = 0;

        if (root != null)
            result = root.count();

        return result;
    }

    /**
     * Returns the height of this tree.
     *
     * @return the height of the tree
     */
    public int getHeight() {
        if (root == null) {
            return 0;
        }
        int leftChildrenHeight = getLeft().getHeight();
        int rightChildrenHeight = getRight().getHeight();

        return Math.max(leftChildrenHeight, rightChildrenHeight) + 1;
    }

    /**
     * Returns the height of the specified node.
     *
     * @param node the node from which to calculate the height
     * @return the height of the tree
     */
    private int height(BinaryTreeNode<T> node) {
        if (node == null) {
            return 0;
        }
        int hleft = height(node.getLeft());
        int hright = height(node.getRight());
        if (hleft > hright) {
            return ++hleft;
        } else
            return ++hright;
    }

    /**
     * Returns true if this tree contains an element that matches the
     * specified target element and false otherwise.
     *
     * @param targetElement the element being sought in this tree
     * @return true if the element in is this tree, false otherwise
     */
    public boolean contains(T targetElement) {
        return (find(targetElement) != null);

    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.  Throws a ElementNotFoundException if
     * the specified target element is not found in the binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @return a reference to the specified target
     * @throws ElementNotFoundException if the element is not in the tree
     */
    public T find(T targetElement) throws ElementNotFoundException {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        return (current.getElement());
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.
     *
     * @param targetElement the element being sought in this tree
     * @param next          the element to begin searching from
     */
    private BinaryTreeNode<T> findNode(T targetElement,
                                       BinaryTreeNode<T> next) {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());

        if (temp == null)
            temp = findNode(targetElement, next.getRight());

        return temp;
    }

    /**
     * Returns a string representation of this binary tree showing
     * the nodes in an inorder fashion.
     *
     * @return a string representation of this binary tree
     */
    public String toString() {
        UnorderedListADT<BinaryTreeNode> nodes =
                new ArrayUnorderedList<BinaryTreeNode>();
        UnorderedListADT<Integer> levelList =
                new ArrayUnorderedList<Integer>();
        BinaryTreeNode current;
        String result = "";
        int printDepth = this.getHeight();
        int possibleNodes = (int) Math.pow(2, printDepth + 1);
        int countNodes = 0;

        nodes.addToRear(root);
        Integer currentLevel = 0;
        Integer previousLevel = -1;
        levelList.addToRear(currentLevel);

        while (countNodes < possibleNodes) {
            countNodes = countNodes + 1;
            current = nodes.removeFirst();
            currentLevel = levelList.removeFirst();
            if (currentLevel > previousLevel) {
                result = result + "\n\n";
                previousLevel = currentLevel;
                for (int j = 0; j < ((Math.pow(2, (printDepth - currentLevel))) - 1); j++)
                    result = result + " ";
            } else {
                for (int i = 0; i < ((Math.pow(2, (printDepth - currentLevel + 1)) - 1)); i++) {
                    result = result + " ";
                }
            }
            if (current != null) {
                result = result + (current.getElement()).toString();
                nodes.addToRear(current.getLeft());
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(current.getRight());
                levelList.addToRear(currentLevel + 1);
            } else {
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                nodes.addToRear(null);
                levelList.addToRear(currentLevel + 1);
                result = result + " ";
            }

        }

        return result;

    }


    /**
     * Returns an iterator over the elements in this tree using the
     * iteratorInOrder method
     *
     * @return an in order iterator over this binary tree
     */
    public Iterator<T> iterator() {
        return iteratorInOrder();
    }

    /**
     * Performs an inorder traversal on this binary tree by calling an
     * overloaded, recursive inorder method that starts with
     * the root.
     *
     * @return an in order iterator over this binary tree
     */
    public Iterator<T> iteratorInOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }

    /**
     * Performs a recursive inorder traversal.
     *
     * @param node     the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void inOrder(BinaryTreeNode<T> node,
                           ArrayUnorderedList<T> tempList) {
        if (node != null) {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }

    /**
     * Performs an preorder traversal on this binary tree by calling
     * an overloaded, recursive preorder method that starts with
     * the root.
     *
     * @return a pre order iterator over this tree
     */
    public Iterator<T> iteratorPreOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }


    /**
     * Performs a recursive preorder traversal.
     *
     * @param node     the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void preOrder(BinaryTreeNode<T> node,
                            ArrayUnorderedList<T> tempList) {
        if (node != null) {
            tempList.addToRear(node.getElement());
            preOrder(node.getLeft(), tempList);
            preOrder(node.getRight(), tempList);
        }
    }

    /**
     * Performs an postorder traversal on this binary tree by calling
     * an overloaded, recursive postorder method that starts
     * with the root.
     *
     * @return a post order iterator over this tree
     */
    public Iterator<T> iteratorPostOrder() {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }

    /**
     * Performs a recursive postorder traversal.
     *
     * @param node     the node to be used as the root for this traversal
     * @param tempList the temporary list for use in this traversal
     */
    protected void postOrder(BinaryTreeNode<T> node,
                             ArrayUnorderedList<T> tempList) {
        if (node != null) {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());

        }
    }

    /**
     * Performs a levelorder traversal on this binary tree, using a
     * templist.
     *
     * @return a levelorder iterator over this binary tree
     */
    public Iterator<T> iteratorLevelOrder() {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty()) {
            current = nodes.removeFirst();

            if (current != null) {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null)
                    nodes.addToRear(current.getLeft());
                if (current.getRight() != null)
                    nodes.addToRear(current.getRight());
            } else
                tempList.addToRear(null);
        }

        return new TreeIterator(tempList.iterator());
    }

    /**
     * Inner class to represent an iterator over the elements of this tree
     */
    private class TreeIterator implements Iterator<T> {
        private int expectedModCount;
        private Iterator<T> iter;

        /**
         * Sets up this iterator using the specified iterator.
         *
         * @param iter the list iterator created by a tree traversal
         */
        public TreeIterator(Iterator<T> iter) {
            this.iter = iter;
            expectedModCount = modCount;
        }

        /**
         * Returns true if this iterator has at least one more element
         * to deliver in the iteration.
         *
         * @return true if this iterator has at least one more element to deliver
         * in the iteration
         * @throws ConcurrentModificationException if the collection has changed
         *                                         while the iterator is in use
         */
        public boolean hasNext() throws ConcurrentModificationException {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }

        /**
         * Returns the next element in the iteration. If there are no
         * more elements in this iteration, a NoSuchElementException is
         * thrown.
         *
         * @return the next element in the iteration
         * @throws NoSuchElementException if the iterator is empty
         */
        public T next() throws NoSuchElementException {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        /**
         * The remove operation is not supported
         *
         * @throws UnsupportedOperationException if the remove operation is called
         */
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

    public BinaryTreeNode<T> makeTree(T[] inorder, int startInorder, int lenInorder, T[] preorder, int startPreorder, int lenPreorder) {
        if (lenInorder < 1) {//判断中序的字符串长度（即元素个数）小于1，则返回null,即树为空
            return null;
        }
        BinaryTreeNode root;//创建根结点
        T rootelement = preorder[startPreorder];//preorder中的第一个元素就是当前处理的数据段的根节点
        root = new BinaryTreeNode(rootelement);//把给定的根结点元素放进root
        int temp;
        boolean isFound = false;
        for (temp = 0; temp < lenInorder; temp++) {
            if (inorder[startInorder + temp] == rootelement) {
                isFound = true;//此时找到结点，即将先序中的第一个元素（根元素）在中序中寻找到相同元素，得到根结点的左右子树
                break;
            }
        }
        if (!isFound)//如果不存在相等的情况就跳出该函数
            return root;//即没有左右子树
        root.setLeft(makeTree(inorder, startInorder, temp, preorder, startPreorder + 1, temp));//递归找到并设置各结点左孩子
        root.setRight(makeTree(inorder, startInorder + temp + 1, lenInorder - temp - 1, preorder, startPreorder + temp + 1, lenPreorder - temp - 1));//递归找到并设置各结点右孩子
        //每次递归，先序序列中都要往后跳过temp个元素（因为他们都是从中序序列中得知的一个结点的左或右孩子），再加1，即为下一个结点的元素
        return root;
    }
    public void buildTree(T[] inorder, T[] postorder) {//调用makeTree方法，即利用递归方法得到树
        BinaryTreeNode temp = makeTree(inorder, 0, inorder.length, postorder, 0, postorder.length);
        root = temp;

    }

}

