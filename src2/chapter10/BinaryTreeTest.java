package chapter10;

import chapter10.TextCode.BinaryTreeADT;
import chapter10.TextCode.BinaryTreeNode;
import chapter10.TextCode.LinkedBinaryTree;

public class BinaryTreeTest {
    public static void main(String[] args) {
        BinaryTreeNode btn1 = new BinaryTreeNode("A");
        BinaryTreeNode btn2 = new BinaryTreeNode("B");
        BinaryTreeNode btn3 = new BinaryTreeNode("C");
        BinaryTreeNode btn4 = new BinaryTreeNode("D");


        LinkedBinaryTree lbt1 = new LinkedBinaryTree(btn1.getElement());
        LinkedBinaryTree lbt2 = new LinkedBinaryTree(btn2.getElement());
        LinkedBinaryTree lbt3 = new LinkedBinaryTree(btn3.getElement(),lbt1,lbt2);
        LinkedBinaryTree lbt4 = new LinkedBinaryTree(btn4.getElement(),lbt3,lbt1);

        System.out.println("输出树："+lbt4);
        System.out.println("树lbt4是否含有结点“B”? :"+lbt4.contains("B"));// PP10.5测试
        lbt4.removeRightSubtree();
        System.out.println("删除右子树(含B)："+lbt4);// PP10.1测试
        System.out.println("树lbt4是否含有结点“B”? :"+lbt4.contains("B"));// PP10.5测试

        System.out.println("B是否为叶子？"+btn2.isleaf());// PP10.3测试

        lbt4.removeAllElements();
        System.out.println("删除所有元素："+lbt4);// PP10.1测试





    }
}
