package chapter05;

import java.util.LinkedList;
import java.util.Queue;

/**
 * @author LbZhang
 * @version 创建时间：2015年11月17日 上午9:08:19
 * @description 使用重复密钥来编码、解码消息 那些整数密钥值存储在一个队列中，使用一个密钥值之后，该值会被重新送回到队尾，
 *              这样较长的消息可以按需继续重复使用该密钥。 注意：队列是一种可存储 重复编码密钥的便利 集合
 * 
 *              解析：这个程序实际使用到了两份密钥，它们分别存储在两个单独的队列中。 思路：消息编码这使用一份密钥，消息解码者使用另一份密钥。
 * 
 */
public class Codes {

	public static void main(String[] args) {

		int[] key = { 5, 12, -3, 8, -9, 4, 10 };

		Integer keyValue;
		String encode = "";
		String decode = "";

		String message = "All programmers are playwrights and all "
				+ "computers are lousy actors.";
		
		Queue<Integer> encodingQueue = new LinkedList<Integer>();
		Queue<Integer> decodingQueue = new LinkedList<Integer>();
		
		/**
		 * 初始化密钥队列
		 */
		for(int i = 0;i<key.length;i++){
			encodingQueue.add(key[i]);
			decodingQueue.add(key[i]);
		}
		
		/**
		 *encode message
		 */
		for(int i=0;i<message.length();i++){
			 keyValue = encodingQueue.remove();
			 encode += (char) ((int)message.charAt(i) + keyValue.intValue());
			 encodingQueue.add(keyValue);
		}
		System.out.println("Encode Message :\n"+encode);
		
		/**
		 *decode message
		 */
		for(int i=0;i<encode.length();i++){
			 keyValue = decodingQueue.remove();
			 decode += (char) ((int)encode.charAt(i) - keyValue.intValue());
			 decodingQueue.add(keyValue);
		}

		System.out.println("Encode Message :\n"+decode);
		
		/**
		 * 改程序在字母表结尾并没有环绕，他能够对unicode字符集中的任何字符编码，会将该字符移动到U字符集的另一个位置 包括标点符号
		 */
	}

}
