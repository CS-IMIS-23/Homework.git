package chapter06;

public class ProductTest {
    public static void main(String[] args) {
        Product product = new Product("Juice",100);
        Product product2 = new Product("Milk",200);
        Product product3= new Product("Cola",300);
        Product product4 = new Product("Fenda",400);

        Productlist productlist = new Productlist();
        productlist.addToFront(product);
        productlist.add(product2);
        productlist.add(product3);
        productlist.addAfter(product4,product);
        System.out.println("添加五个商品：\n"+productlist);

        System.out.println("有芬达没？"+productlist.find(product4));

        productlist.remove(product4);
        System.out.println("删去芬达：\n"+productlist);
        System.out.println("有芬达没？"+productlist.find(product4));


        productlist.SelectionSort();
        System.out.println("选择排序后：\n"+productlist);

    }
}
