package chapter06;

public class LinkedOrderedList<T> extends LinkedList<T> implements
		OrderedListADT<T> {
	public LinkedOrderedList()
	{
		super();
	}

	@Override
	public void add(T element) {
		LinearNode<T> temp = new LinearNode(element);
		LinearNode<T> previous = null;
		LinearNode<T> current = head;
		while (current != null && Integer.parseInt(element + "") > Integer.parseInt(current.getElement() + "")) {
			previous = current;
			current = current.getNext();
		}
		if (previous == null) {
			head = tail = temp;
		} else {
			previous.setNext(temp);
		}
		temp.setNext(current);
		if (temp.getNext() == null)
			tail = temp;


		count++;
		modCount++;
	}

//	public String toString() {
//
//
//	}
}


