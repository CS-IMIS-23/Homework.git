package chapter06;

import java.util.LinkedList;
import java.util.List;

public class ArrayOrderedCourse {
    public static void main(String[] args) {
        ArrayOrderedCourse aoc = new ArrayOrderedCourse();
        Course a = new Course("CS", 101, "Introduction to Programming", "A-");
        Course b = new Course("ARCH", 305, "Building Analysis", "A");
        Course c = new Course("GER", 210, "Intermediate German");

        Course d = a;

        if (a.compareTo(b) == 1)
        {
            a = b;
            b = d;
        }
        if (a.compareTo(c) == 1){
            a = c;
            c = d;
        }
        if (b.compareTo(c) == 1){
            d = c;
            b = c;
            c = d;
        }
        aoc.addCourse(a);
        aoc.addCourse(b);
        aoc.addCourse(c);
        System.out.println("简单排序一下：\n"+aoc);
        System.out.println();
        aoc.find("CS", 101);
        aoc.addCourseAfter(a,c);
        aoc.replace(c,b);
        System.out.println(aoc);






    }
    private List<Course> list;

    public ArrayOrderedCourse() {
        list = new LinkedList<Course>();
    }


    public void addCourse(Course course) {
        if (course != null)
            list.add(course);
    }

    public Course find(String prefix, int number) {
        for (Course course : list)
            if (prefix.equals(course.getPrefix())
                    && number == course.getNumber())
                return course;

        return null;
    }

    /**
     * 在某个元素之后添加元素
     * @param target
     * @param newCourse
     */
    public void addCourseAfter(Course target, Course newCourse) {
        if (target == null || newCourse == null)
            return;

        int targetIndex = list.indexOf(target);//获取索引
        if (targetIndex != -1)
            list.add(targetIndex + 1, newCourse);
    }

    public void replace(Course target, Course newCourse) {
        if (target == null || newCourse == null)
            return;

        int targetIndex = list.indexOf(target);
        if (targetIndex != -1)
            list.set(targetIndex, newCourse);
    }

    public String toString() {
        String result = "";

        for (Course course : list)
            result += course + "\n";
        return result;
    }
}
