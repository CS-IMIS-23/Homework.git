package chapter06;

public class Product implements Comparable<Product>{
    private String name;
    private int price;

    public Product(String name, int price) {
        super();
        this.name = name;
        this.price = price;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String toString()
    {
        String result = name + " " + price + "$\n";
        return result;
    }

    @Override
    public int compareTo(Product o) {
        if (name.compareTo(o.getName()) > 0)
            return 1;
        else{
                if (price > o.getPrice())
                    return 1;
                else
                    return -1;

        }
    }

}
