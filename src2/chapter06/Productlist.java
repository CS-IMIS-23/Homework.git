package chapter06;

import java.util.List;

public class Productlist<T> extends LinkedList<T>{
    private List<Product> list = new java.util.LinkedList<Product>();

    public LinearNode<T> SelectionSort(){
        LinearNode<T> curNode = head;
        while(curNode != null){
            LinearNode<T> nextNode = curNode.next;
            while(nextNode != null){
                if(((Product)curNode.getElement()).compareTo( (Product)nextNode.getElement())>0){
                    T temp = curNode.getElement();
                    curNode.setElement(nextNode.getElement());
                    nextNode.setElement(temp);
                }
                nextNode = nextNode.next;
            }
            curNode = curNode.next;
        }
        return head;
    }

    public T remove(T targetElement) {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        // 使用两个引用遍历
        LinearNode<T> previous = null;// 前一结点的设置
        LinearNode<T> current = head;// 当前的结点

        while (current != null && !found)
            if (targetElement.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        // 如果没有发现需要删除的结点
        if (!found)
            throw new ElementNotFoundException("LinkedList");

        if (size() == 1) // only one element in the list
            head = tail = null;
        else if (current.equals(head)) // target is at the head
            head = current.getNext();
        else if (current.equals(tail)) // target is at the tail
        {
            tail = previous;
            tail.setNext(null);
        } else
            // target is in the middle
            previous.setNext(current.getNext());

        count--;
        modCount++;

        return current.getElement();
    }

    public void add(Product product) {
        LinearNode<T> temp = new LinearNode(product);
        LinearNode<T> previous = null;
        LinearNode<T> current = head;
        while (current != null && product.compareTo((Product)current.getElement())>0) {
            previous = current;
            current = current.getNext();
        }
        if (previous == null) {
            head = tail = temp;
        } else {
            previous.setNext(temp);
        }
        temp.setNext(current);
        if (temp.getNext() == null)
            tail = temp;


        count++;
        modCount++;
    }

    public void addToFront(Product product)
    {
        LinearNode<T> temp = new LinearNode(product);
        if (count == 0)
            head = tail = temp;
        else {
            temp.setNext(head);
            head = temp;
        }
        count++;
    }

    public boolean find(Product product) {
        if (isEmpty())
            throw new EmptyCollectionException("LinkedList");

        boolean found = false;
        LinearNode<Product> previous = null;
        LinearNode<Product> current = (LinearNode<Product>) head;

        while (current != null && !found)
            if (product.equals(current.getElement()))
                found = true;
            else {
                previous = current;
                current = current.getNext();
            }

        if (!found)
            return false;
        else
            return true;
    }

    public void addToRear(Product product) {
        LinearNode<T> temp = new LinearNode(product);
        if (count == 0)
            head = tail = temp;
        else {
            tail.setNext(temp);
            tail = temp;
        }
        count++;
    }
    public void addAfter(Product newproduct, Product target)
    {
        LinearNode<T> node1 = new LinearNode(newproduct);
        LinearNode<T> current = head;
        while(current.getElement() != target)
            current = current.getNext();
        node1.setNext(current.getNext());
        current.setNext(node1);
        if(node1.getNext() == null)
            tail = node1;
        count++;
    }
}
