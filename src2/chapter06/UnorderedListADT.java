package chapter06;

public interface UnorderedListADT<T> extends Iterable<T> {
	public void addToFront(T element);

	public void addToRear(T element);

	public void addAfter(T element, T target);

	public T removeFirst();

}
