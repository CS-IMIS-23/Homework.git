package chapter06;

import java.io.Serializable;

public class Course implements Serializable,Comparable<Course>{
	
	private String prefix;
	private int number;
	private String title;
	private String grade;
	public Course(String prefix, int number, String title, String grade) {
		super();
		this.prefix = prefix;
		this.number = number;
		this.title = title;
		if(grade==null){
			this.grade = "";
		}else{
			this.grade = grade;
		}
		
	}
	
	public Course(String prefix, int number, String title)
	{
		this(prefix, number, title, "");
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public int getNumber() {
		return number;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}
	/**
	 * Returns true if this course has been taken  
	 * 
	 * @return true if this course has been taken and false otherwise
	 */
	public boolean taken()
	{
		return !grade.equals("");
	}

	public boolean equals(Object other)
	{
		boolean result = false;
		if (other instanceof Course)
		{
			Course otherCourse = (Course) other;
			if (prefix.equals(otherCourse.getPrefix()) &&
					number == otherCourse.getNumber())
				result = true;
		}
		return result;
	}
	
	
	public String toString()
	{
		String result = prefix + " " + number + ": " + title;
		if (!grade.equals(""))
			result += "  [" + grade + "]";
		return result;
	}

	@Override
	public int compareTo(Course o) {
		if (prefix.compareTo(o.getPrefix()) > 0)
			return 1;
		else{
			if (title.compareTo(o.getPrefix()) < 0)
				return -1;
			else{
				if (number > o.getNumber())
					return 1;
				else
					return -1;
			}
		}
	}
}
