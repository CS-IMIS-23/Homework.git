package chapter06;
/**
 * @author LbZhang
 * @version 创建时间：2015年11月17日 下午9:06:03
 * @description 类说明
 */
public class LinkedUnorderedList<T> extends LinkedList<T> implements
		UnorderedListADT<T> {

	@Override
	public T removeFirst() {
		// TODO Auto-generated method stub
		return super.removeFirst();
	}

	@Override
	public T removeLast() {
		// TODO Auto-generated method stub
		return super.removeLast();
	}

	public LinkedUnorderedList()
	{
		super();
	}


	public void addToFront(T element)
	{
		LinearNode<T> temp = new LinearNode<>(element);
		if (count == 0)
			head = tail = temp;
		else {
			temp.setNext(head);
			head = temp;
		}
		count++;
	}


	public void addToRear(T element) {
		LinearNode<T> temp = new LinearNode<>(element);
		if (count == 0)
			head = tail = temp;
		else {
			tail.setNext(temp);
			tail = temp;
		}
		count++;
	}



	public void addAfter(T element, T target)
	{
		LinearNode<T> node1 = new LinearNode<>(element);
		LinearNode<T> current = head;
		while(current.getElement() != target)
			current = current.getNext();
		node1.setNext(current.getNext());
		current.setNext(node1);
		if(node1.getNext() == null)
			tail = node1;
		count++;
	}
	

}
