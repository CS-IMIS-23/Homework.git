package chapter06;

public class LinkedListTest {
    public static void main(String[] args) {
        System.out.println("测试LinkedOrderedList类:");
        LinkedOrderedList lol = new LinkedOrderedList();//测试LinkedOrderedList类
        System.out.println("添加五个元素：");
        lol.add(4);
        lol.add(2);
        lol.add(3);
        lol.add(39);
        lol.add(323);
        System.out.println(lol);
        System.out.println("删去3：");
        lol.remove(3);
        System.out.println(lol);
        System.out.println("是否含3？");
        System.out.println(lol.contains(3));
        System.out.println("去头去尾：");
        lol.removeFirst();
        lol.removeLast();
        System.out.println(lol);

        System.out.println("测试LinkedUnorderedList类:");
        LinkedUnorderedList lul = new LinkedUnorderedList();
        System.out.println("添加三个元素：");
        lul.addToFront(12);
        lul.addToRear(23);
        lul.addToRear(34);
        System.out.println(lul);
        System.out.println("23后面添加5");
        lul.addAfter(5,23);
        System.out.println(lul);
        System.out.println("删去12：");
        lul.remove(12);
        System.out.println(lul.contains(3));
        System.out.println("去头去尾：");
        lul.removeFirst();
        lul.removeLast();
        System.out.println(lul);
    }
}
