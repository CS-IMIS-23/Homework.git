package chapter12.Practice;

/**
 * HeapSort sorts a given array of Comparable objects using a heap.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class HeapSort<T> {
	/**
	 * Sorts the specified array using a Heap
	 *
	 * @param data the data to be added to the heapsort
	 */
	public void HeapSort(T[] data) {
		ArrayHeap<T> temp = new ArrayHeap<T>();

		// copy the array into a heap
		for (int i = 0; i < data.length; i++)
			temp.addElement(data[i]);
		System.out.println("层序遍历输出大顶堆："+temp.toString());


		// place the sorted elements back into the array
		int count = 0;
		while (!(temp.isEmpty())) {
			data[count] = temp.removeMax();
			count++;
			System.out.print("第"+count+"轮排序：");
//			for(int a = 0;a < data.length;a++){
//				System.out.print(data[a]+" ");
//			}
//			System.out.println();
			System.out.println(temp.toString());
		}
	}


	public static void main(String[] args) {
		ArrayHeap temp = new ArrayHeap();
		Integer[] data = {36,30,18,40,32,45,22,50};
		HeapSort hs = new HeapSort<Integer>();
		hs.HeapSort(data);

		System.out.println("排序结果：");
		for(int i=0;i<data.length;i++){
			System.out.print(data[i]+" ");
		}






	}
}


