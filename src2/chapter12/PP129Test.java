package chapter12;

public class PP129Test {
    public static void main(String[] args) {
        LinkedHeap linkedHeap = new LinkedHeap();
        linkedHeap.addElement(1);
        linkedHeap.addElement(2);
        linkedHeap.addElement(3);
        linkedHeap.addElement(4);
        linkedHeap.addElement(5);
        linkedHeap.addElement(6);
        linkedHeap.addElement(7);
        System.out.println(linkedHeap);
        linkedHeap.removeMin();
        System.out.println("删去最小之后：");
        System.out.println(linkedHeap);
        System.out.println("最小值为："+linkedHeap.findMin());


    }
}
