package chapter12;

public class PP128Test {
    public static void main(String[] args) {
        ArrayHeap arrayHeap = new ArrayHeap();
        arrayHeap.addElement(1);
        arrayHeap.addElement(2);
        arrayHeap.addElement(3);
        arrayHeap.addElement(4);
        arrayHeap.addElement(5);
        arrayHeap.addElement(6);
        arrayHeap.addElement(7);
        System.out.println(arrayHeap);
        System.out.println("删去最大值之后："+arrayHeap.findMin());
        arrayHeap.removeMax();

        System.out.println("最大值为: "+arrayHeap.findMax());
    }
}
