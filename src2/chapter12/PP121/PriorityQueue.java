package chapter12.PP121;

import chapter12.ArrayHeap;

/**
 * 优先队列使用堆实现优先队列。
 */
public class PriorityQueue<T> extends ArrayHeap<PrioritizedObject<T>>
{
    /**
     * Creates an empty priority queue.
     */
    public PriorityQueue() 
    {
        super();
    }
    
    /**
     * Adds the given element to this PriorityQueue.
     *
     * @param object the element to be added to the priority queue
     */
    public void add(T object)
    {
        PrioritizedObject<T> obj = new PrioritizedObject<T>(object);
	    super.addElement(obj);
    }
    
    /**
     * 从优先级队列中删除下一个最高优先级元素并返回对它的引用
     * @return a reference to the next highest priority element in this queue
     */
    public T removeNext() 
    {
        PrioritizedObject<T> obj = (PrioritizedObject<T>)super.removeMin();
        return obj.getElement();
    }
}


