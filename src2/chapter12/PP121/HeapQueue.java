package chapter12.PP121;

import chapter12.ArrayHeap;

/**
 * 优先队列使用堆实现优先队列。
 */
public class HeapQueue<T> extends ArrayHeap<HeapQueueObject<T>>
{
    /**
     * Creates an empty priority queue.
     */
    public HeapQueue()
    {
        super();
    }
    

    public void add(T object)
    {
        HeapQueueObject<T> obj = new HeapQueueObject<T>(object);
	    super.addElement(obj);
    }

    public T removeNext() 
    {
        HeapQueueObject<T> obj = (HeapQueueObject<T>)super.removeMin();
        return obj.getElement();
    }
}


