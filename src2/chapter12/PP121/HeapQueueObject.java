package chapter12.PP121;

/**
 * PrioritizedObject表示优先队列中的一个节点，包含一个可比较的对象、到达顺序和优先级值
 */
public class HeapQueueObject<T> implements Comparable<HeapQueueObject>
{
    private static int nextOrder = 0;
    private int arrivalOrder;
    private T element;

    /**
     * 使用指定的数据创建一个新的优先级对象。
     *
     * @param element the element of the new priority queue node
     */
    public HeapQueueObject(T element)
    {
        this.element = element;
        arrivalOrder = nextOrder;
        nextOrder++;
    }
    
    /**
     * Returns the element in this node.
     *
     * @return the element contained within the node
     */
    public T getElement() 
    {
        return element;
    }

    /**
     * Returns the arrival order for this node.
     *
     * @return the integer arrival order for this node
     */
    public int getArrivalOrder() 
    {
        return arrivalOrder;
    }
    
    /**
     * Returns a string representation for this node.
     *
     */
    public String toString() 
    {
        return (element + " ");
    }
    
    /**
     * 如果此对象的优先级高于给定对象，则返回1，否则返回-1。
     * @param obj the object to compare to this node
     * @return 将给定对象与此对象进行比较的结果
     */
    public int compareTo(HeapQueueObject obj)
    {
      int result;

      if (arrivalOrder > obj.getArrivalOrder())
          result = 1;
      else
          result = -1;
      
      return result;
    }
}


