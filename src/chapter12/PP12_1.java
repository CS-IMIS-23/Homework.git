package chapter12;

import java.util.Scanner;

public class PP12_1 {
    static String str;
    public static void main(String[] args) {

        int n, m;

        Scanner scan = new Scanner(System.in);
        System.out.println("enter a potential palindrome: ");
        str = scan.nextLine();

        n = 0;
        m = str.length() - 1;
        Fact(n, m);

    }
        public static int Fact ( int n, int m){

            if (n < m) {
                if (String.valueOf(str.charAt(n)).equals(String.valueOf(str.charAt(m))))
                    return Fact(n + 1, m - 1);
                else
                    System.out.println("that string is not a palindrome");
            } else
                System.out.println("that string is a palindrome");

            return n;
        }
}

