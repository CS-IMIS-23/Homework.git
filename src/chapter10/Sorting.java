package chapter10;//********************************************************************
//  Sorting.java       Author: Lewis/Loftus
//
//  Demonstrates the selection sort and insertion sort algorithms.
//********************************************************************

public class Sorting
{
   //-----------------------------------------------------------------
   //  Sorts the specified array of objects using the selection
   //  sort algorithm.
   //-----------------------------------------------------------------
   public static void selectionSort(Comparable[] list)
   {
      int min;
      int nzhouyajie = list.length;
      Comparable temp;

      for (int index = 0; index < list.length-1; index++) {
         min = index;
         for (int scan = index + 1; scan < list.length; scan++)
            if ((Integer.parseInt((String)list[scan])) <= (Integer.parseInt((String)(list[min]))))//??????????????????????
               min = scan;

         // Swap the values
         temp = list[min];
         list[min] = list[index];
         list[index] = temp;
         for (int a = 0; a < list.length; a++) //打印数组
            System.out.print(list[a]+" ");
         System.out.println("第"+(index+1)+"次交换;"+"一共有"+nzhouyajie+"个元素");



      }

   }

   //-----------------------------------------------------------------
   //  Sorts the specified array of objects using the insertion
   //  sort algorithm.
   //-----------------------------------------------------------------
   public static void insertionSort(Comparable[] list)
   {
      for (int index = 1; index < list.length; index++)
      {
         Comparable key = list[index];
         int position = index;

         //  Shift larger values to the right
         while (position > 0 && key.compareTo(list[position-1]) < 0)
         {
            list[position] = list[position-1];
            position--;
         }
            
         list[position] = key;
      }
   }

}
