package chapter10;//********************************************************************
//  Firm2.java       Author: Lewis/Loftus
//
//  Demonstrates polymorphism via inheritance.
//********************************************************************

public class Firm2
{
   //-----------------------------------------------------------------
   //  Creates a staff of employees for a firm and pays them.
   //-----------------------------------------------------------------
   public static void main(String[] args)
   {
      Payable personnel = new Staff2() {
          @Override
          public double pay() {
              return 0;
          }
      };

      personnel.payday();
   }
}
