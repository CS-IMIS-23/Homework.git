package chapter10;//********************************************************************
//  PhoneList2.java       Author: Lewis/Loftus
//
//  Driver for testing a sorting algorithm.
//********************************************************************

public class PhoneList2
{
   //-----------------------------------------------------------------
   //  Creates an array of Contact objects, sorts them, then prints
   //  them.
   //-----------------------------------------------------------------
   public static void main(String[] args)
   {
      Contact2[] friends = new Contact2[8];

      friends[0] = new Contact2("John", "Smith", "610-555-7384");
      friends[1] = new Contact2("Sarah", "Barnes", "215-555-3827");
      friends[2] = new Contact2("Mark", "Riley", "733-555-2969");
      friends[3] = new Contact2("Laura", "Getz", "663-555-3984");
      friends[4] = new Contact2("Larry", "Smith", "464-555-3489");
      friends[5] = new Contact2("Frank", "Phelps", "322-555-2284");
      friends[6] = new Contact2("Mario", "Guzman", "804-555-9066");
      friends[7] = new Contact2("Marsha", "Grant", "243-555-2837");

      Sorting2.selectionSort(friends);

      for (Contact2 friend : friends)
         System.out.println(friend);

      System.out.println();

      Sorting2.selectionSort1(friends);

      for (Contact2 friend : friends)
         System.out.println(friend);
   }
}
