package chapter10;

public interface Payable {
    public double pay();
    public void payday();
    public String toString();


}
