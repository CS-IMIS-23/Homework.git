    public class PP17
{
	public static void main(String[] args)
	{
System.out.println( "  我的天空里没有太阳，总是黑夜，但并不暗，因为");
System.out.println("有东西代替了太阳。虽然没有太阳那么明亮，但对我");
System.out.println("来说已经足够。凭借着这份光，我便能把黑夜当成白");
System.out.println("天。我从来就没有太阳，所以不怕失去。");
	}
}
