    public class CoinFlip
{
	public static void main(String[] args)
	{
		Coin myCoin=new Coin();

		myCoin.flip();

		System.out.println(myCoin);

		if (myCoin.isHeads())
			System.out.println("you win.");
		else
			System.out.println("Better luck next time.");
	}
}
