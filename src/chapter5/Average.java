    import java.text.DecimalFormat;
    import java.util.Scanner;

    public class Average
{
	public static void main(String[] args)
	{
		int sum=0,value,count=0;
		double average;

		Scanner scan =new Scanner(System.in);

		System.out.print("enter an integer (0 to quit): ");
		value =scan.nextInt();

		while (value !=0)
		{
			count++;

			sum += value;
			System.out.println("the sum so far is "+sum);
			value=scan.nextInt();
		}

		System.out.println();

		if (count==0)
			System.out.println("no values were entered.");
		else
		{
			average=(double)sum/count;

			DecimalFormat fmt=new DecimalFormat("0.###");
		System.out.println("the average is "+fmt.format(average));
		}
	}
}
