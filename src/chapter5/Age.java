    import java.util.Scanner;

    public class Age
{
	public static void main(String[] args)
	{
		final int MINOR=21;

		Scanner scan=new Scanner(System.in);
		System.out.println("enter your age: ");
		int age =scan.nextInt();

		System.out.println("you entered: "+age);

		if (age<MINOR)
			System.out.println("youth is a wonderful thing. enjoy");
		System.out.println("Age is a state of mind.");
	}
}
