    import java.util.Scanner;

    public class ReverseOrder
{
	public static void main(String[] args)
	{
		Scanner scan=new Scanner(System.in);

		double[] numbers=new double[10];

		System.out.println("the size of the array: "+numbers.length);

		for (int index=0;index<numbers.length;index++)
		{
			System.out.print("enter number "+(index+1)+": ");
			numbers[index]=scan.nextDouble();
		}
		System.out.println("the numbers in reverse order:");

		for (int index=numbers.length-1;index>=0;index--)
			System.out.println(numbers[index]+" ");
	}
}
