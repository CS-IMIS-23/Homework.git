    public class Primes
{
	public static void main(String[] args)
	{
		int[] primeNums={2,3,5,7,11,13,17,19};

		System.out.println("array length: "+primeNums.length);

		System.out.println("the first few prime numbers are: ");
		
		for (int prime : primeNums)
			System.out.println(prime+" ");
	}
}
