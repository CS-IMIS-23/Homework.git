package chapter3;

import java.util.Random;
public class PP39 {
    public static void main(String[] args) {
        Random generator=new Random();
        int r,h;
        double v,s;

        r=generator.nextInt(20)+1;
        h=generator.nextInt(20)+1;

        v=Math.PI*Math.pow(r,2)*h;
        s=2*Math.PI*r*h;

        System.out.println("体积:"+v+"表面积:"+s);
    }
}
