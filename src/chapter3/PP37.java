package chapter3;

import java.text.DecimalFormat;
import java.util.Scanner;
public class PP37 {
    public static void main(String[] args) {
        double num1,s,a,b,c;
        Scanner scan=new Scanner(System.in);

        System.out.println("please enter a: ");
        a=scan.nextDouble() ;

        System.out.println("please enter b: ");
        b=scan.nextDouble() ;

        System.out.println("please enter c: ");
        c=scan.nextDouble() ;

        s=(a+b+c)/2.0;
        num1=Math.sqrt(s*(s-a)*(s-b)*(s-c));

        DecimalFormat ftm=new DecimalFormat("0.###") ;

        System.out.println("the area is: "+ftm.format(num1));
    }
}
