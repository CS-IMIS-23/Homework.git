package chapter3;

import java.text.DecimalFormat;
import java.util.Scanner;
public class PP36 {
    public static void main(String[] args) {
        double num1,num2,num3;
        final double NUM=4.0/3;
        Scanner scan =new Scanner(System.in);

        System.out.println("please enter the number of r: ");
        num1=scan.nextDouble() ;

        num2=NUM*Math.PI*Math.pow(num1,3);
        num3=4*Math.PI*Math.pow(num1,2);

        DecimalFormat ftm=new DecimalFormat("0.####") ;
        System.out.println("体积: "+ftm.format(num2)+"表面积："+ftm.format(num3));

    }
}
