package chapter3;

import java.util.Scanner;
    import java.text.NumberFormat;

    public class Eg34
{
	public static void main(String[] args)
	{
		final double TAX_RARE=0.06;
		int quantity;
		double subtotal,tax,totalCost,unitPrice;

		Scanner scan=new Scanner(System.in);

		NumberFormat fmt1=NumberFormat.getCurrencyInstance();
		NumberFormat fmt2=NumberFormat.getPercentInstance();

		System.out.print("enter the quantity: ");
		quantity=scan.nextInt();

		System.out.print("enter the unit price: ");
		unitPrice=scan.nextDouble();

		subtotal=quantity*unitPrice;
		tax=subtotal*TAX_RARE;
		totalCost=subtotal+tax;

		System.out.println("Subtotal: "+fmt1.format(subtotal));
		System.out.println("tax: "+fmt1.format(tax)+" at "+
				fmt2.format(TAX_RARE));
		System.out.println("Total: "+fmt1.format(totalCost));




	}
}


