    import java.util.Scanner;

    public class PP211
{
	public static void main(String[] args)
	{
		double sum;
		int ten, five, one,quarter, dime, nickle, penny;

		Scanner scan =new Scanner(System.in);

		System.out.print("Please enter the sum of the money: ");
		sum=scan.nextDouble();

		ten=(int)(sum/10);
		five=(int)(sum-ten*10)/5;
		one=(int)(sum-ten*10-five*5);
		quarter=(int)((sum-(int)sum)*4);
		dime=(int)(((sum-(int)sum)-quarter*0.25)*10);
		nickle=(int)(((sum-(int)sum)-quarter*0.25-dime*0.1)*20);
		penny=(int)((sum*10-(int)(sum*10))*10);

	System.out.println("the result is:"+"\n\t"+ten+" ten dollar bills"+
			"\n\t"+five+" five dollar bills"+
			"\n\t"+one+" one dollar bills"+
			"\n\t"+quarter+" quarters"+"\n\t"+dime+" dimes"+
			"\n\t"+nickle+" nickles"+"\n\t"+penny+" pennies");
	}
}
