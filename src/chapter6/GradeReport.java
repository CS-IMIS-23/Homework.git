    import java.util.Scanner;

    public class GradeReport
{
	public static void main(String[] args)
	{
		int grade,category;

		Scanner scan=new Scanner(System.in);

		System.out.print("enter a numeric grade (0 to 100): ");
		grade=scan.nextInt();
		
		category=grade/10;

		System.out.print("that grade is ");

		switch (category)
		{
			case 10:
				System.out.println("a perfect score.well done");
				break;
			case 9:
				System.out.println("above average.excellent");
				break;

			case 8:
				System.out.println("above average.nice job.");
				break;

			case 7:
				System.out.println("average.");
				break;

			case 6:
				System.out.println("below average.you should"+
				"see the\ninstructor to clarify the material"+
				"\npresented in class.");
				break;
			default:
				System.out.println("not passing.");
		}
	}
}


			
			

