package test;

import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import test4.experiment1.Sorting;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

public class SortingTest {
/** 
* 
* Method: selectionSort(T[] data) 
* 
*/
    private static final Integer[] a = {3, 2, 6, 4, 5, 1, 7, 8, 9, 2308};
    private static final Integer[] c = {3, 2, 6, 4, 5, 1, 2308, 7, 8, 9};
    private static final Integer[] b = {9, 8, 7, 6, 2308, 5, 4, 3, 2, 1};

@Test
public void testNormalSelectionSort() throws Exception {

    assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(a));
    assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(b));
    assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(c));
}
    @Test
    public void testAbnormalSelectionSort() throws Exception {

        String[] p = {"A","B","C","D"};
        Integer[] q = {3, 2, 6, 4, 5, 1, 2308, 7, 8, 9};
        Integer[] a = {1};
        Integer[] b = {2};
        Integer[] c = {3};

        assertEquals("A B C D ", Sorting.selectionSort(p));

        //因为排序的方法结果肯定不会出现异常，所以这里改了对比用的正确结果为错误的，所以一定无法通过
        assertEquals("1o 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(q));


    }
    @Test
    public void testOrderSelectionSort() throws Exception {

        assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(a));
        assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(b));
        assertEquals("1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(c));
    }
    @Test
    public void testBoundarySelectionSort() throws Exception {
        Integer[] m = {3, 2, 6, 4, 5, 1, 7, 8, 9, 2308,999999999};
        Integer[] n = {3, 2, 6, 4, 5, 1, 7, 8, 9, 2308,0};

        assertEquals("1 2 3 4 5 6 7 8 9 2308 999999999 ", Sorting.selectionSort(m));
        assertEquals("0 1 2 3 4 5 6 7 8 9 2308 ", Sorting.selectionSort(n));

    }
    @Test
    public void testReverseSelectionSort() throws Exception {
        Sorting.selectionSort(a);
        assertEquals("2308 9 8 7 6 5 4 3 2 1 ",Sorting.result2);
        Sorting.selectionSort(b);//出现这样的排序结果是因为数组b是在前一个数组a后面接上去的，数组c也是一样
        assertEquals("2308 9 8 7 6 5 4 3 2 1 2308 9 8 7 6 5 4 3 2 1 ", Sorting.result2);
        Sorting.selectionSort(c);
        assertEquals("2308 9 8 7 6 5 4 3 2 1 2308 9 8 7 6 5 4 3 2 1 2308 9 8 7 6 5 4 3 2 1 ", Sorting.result2);
    }

}
