package test;

import org.junit.Test; 
import org.junit.Before; 
import org.junit.After;
import test4.experiment1.Searching;

import static org.junit.Assert.assertEquals;

/** 
* Searching Tester. 
* 
* @author <Authors name> 
* @since <pre>ʮһ�� 19, 2018</pre> 
* @version 1.0 
*/ 
public class SearchingTest {

    private static final Integer[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};//正序
    private static final Integer[] b = {10, 9, 8, 7, 6, 5, 4, 3, 2, 1};//逆序

@Test
public void testNormalLinearSearch1() throws Exception {

    assertEquals(true, Searching.linearSearch(a,0,9,4));
    assertEquals(true, Searching.linearSearch(a,0,4,1));
    assertEquals(true, Searching.linearSearch(a,4,9,9));
}
    @Test
    public void testAbnormalLinearSearch1() throws Exception {

        assertEquals(false, Searching.linearSearch(a,0,9,11));
        assertEquals(false, Searching.linearSearch(a,0,4,7));
        assertEquals(false, Searching.linearSearch(a,4,9,1));
    }
    @Test
    public void testBoundaryLinearSearch1() throws Exception {

        assertEquals(true, Searching.linearSearch(a,0,9,1));
        assertEquals(true, Searching.linearSearch(a,0,4,5));
        assertEquals(true, Searching.linearSearch(a,4,9,10));
    }
    @Test
    public void testNormalLinearSearch2() throws Exception {

        assertEquals(true, Searching.linearSearch(b,0,9,4));
        assertEquals(true, Searching.linearSearch(b,0,4,9));
        assertEquals(true, Searching.linearSearch(b,4,9,2));
    }
    @Test
    public void testAbnormalLinearSearch2() throws Exception {

        assertEquals(false, Searching.linearSearch(b,0,9,11));
        assertEquals(false, Searching.linearSearch(b,0,4,1));
        assertEquals(false, Searching.linearSearch(b,4,9,10));
    }
    @Test
    public void testBoundaryLinearSearch2() throws Exception {

        assertEquals(true, Searching.linearSearch(b,0,9,1));
        assertEquals(true, Searching.linearSearch(b,0,4,6));
        assertEquals(true, Searching.linearSearch(b,4,9,1));
    }


} 
