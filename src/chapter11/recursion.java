package chapter11;

import java.io.*;
import java.util.Scanner;

public class recursion {
    public static void main(String[] args) throws IOException {
        int n,result;
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入一个整数n: ");
        n=scan.nextInt();//----------------输入n值

        result=fact(n);
        System.out.println("函数的结果为: "+result);

        File file=new File("D:\\Code","recursion.txt");
        if (!file.exists())
        {
            file.createNewFile();
        }
        OutputStream outputStream1=new FileOutputStream(file);//---------创建文件
        String result1=String.valueOf(result);
        outputStream1.write(result1.getBytes());//-------------将结果写入文件

    }
    public static int fact(int n){
        if (n==0)
            return 0;
        else {
            if (n == 1)
                return 1;
            else
                return fact(n-1)+fact(n-2);
        }
    }//-----------递归方法------------
}
