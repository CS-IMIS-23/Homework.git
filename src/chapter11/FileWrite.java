package chapter11;

import java.io.*;
import java.util.Scanner;

public class FileWrite {
    public static void main(String[] args) throws IOException {
        int a = 0;
        int[] num = new int[10];
        String judge = "n";
        Scanner scan = new Scanner(System.in);
        Scanner Scan = new Scanner(System.in);

        File file = new File("D:\\Code", "sort.txt");

        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);//--------创建文件

        for (a = 0; judge.equals("n"); a++) {
            System.out.print("请输入数字: ");
            num[a] = scan.nextInt();

            outputStream1.write((String.valueOf(num[a])+" ").getBytes());

            System.out.print("是否结束输入(y/n): ");
            judge = Scan.nextLine();
        }//------------------------输入任意多个整数
        outputStream1.write((String.valueOf("排序后：").getBytes()));

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available() > 0) {
            System.out.print((int) inputStream1.read() + " ");
        }
        inputStream1.close();//------------文件读取-------

        int min, temp;

        for (int index = 0; index < a; index++) {
            min = index;
            for (int b = index + 1; b < a; b++)
                if (num[b] < (num[min]))
                    min = b;
            // Swap the values
            temp = num[min];
            num[min] = num[index];
            num[index] = temp;//-----------选择法排序----------

            FileWriter fw = null;
            try {
             //-----------如果文件存在，则追加内容；如果文件不存在，则创建文件
                File f = new File("D:\\Code", "sort.txt");
                fw = new FileWriter(f, true);
            } catch (IOException e) {
                e.printStackTrace();
            }
            PrintWriter pw = new PrintWriter(fw);

            pw.print(" "+num[index]);//"追加内容"
            pw.flush();
            try {
                fw.flush();
                pw.close();
                fw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


    }

