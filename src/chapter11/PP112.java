package chapter11;

import java.util.Scanner;

public class PP112 {
        public static void main(String[] args){
            int count = 0, banned = 0;
            String str;
            final String DONE;

            StringTooLongException problem = new StringTooLongException("the message is too long.");

            Scanner scan = new Scanner(System.in);
            System.out.println("please enter the message(end up with DONE): ");
            str= scan.nextLine();

            for (count = 0; !str.equals("DONE"); count++) {

                try {
                    if (str.length() > 20)
                        throw problem;
                }
                catch (StringTooLongException exception)
                {
                    System.out.println("the message "+"\""+str+"\""+" is too long.");
                }

                if (str.equals("DONE"))
                    System.out.println();
                else {
                    System.out.println("please enter the message(end up with DONE): ");
                    str= scan.nextLine();
                }
            }
        }
}

