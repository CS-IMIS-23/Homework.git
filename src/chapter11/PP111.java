package chapter11;

import java.util.Scanner;

public class PP111 {
    public static void main(String[] args) throws StringTooLongException {

        String str;
        final String DONE;

        StringTooLongException problem = new StringTooLongException("the message is too long.");


        Scanner scan = new Scanner(System.in);
        System.out.println("please enter the message(end up with DONE): ");
        str= scan.nextLine();

        for (int count = 0; !str.equals("DONE"); count++) {

            if (str.length() > 20)
                throw problem;

            if (str.equals("DONE"))
                System.out.println();
            else {
                System.out.println("please enter the message(end up with DONE): ");
                str= scan.nextLine();
            }
        }
    }
}

