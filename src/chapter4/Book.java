    public class Book
{
	private String bookname;
	private String author;
	private String press;
	public String copylightdate;

	public Book(String bn,String au,String pr,String cld)
	{
		bookname=bn;
		author=au;
		press=pr;
		copylightdate=cld;
	}
	public String getBookName()
	{
		return "书名: "+bookname;
	}
	public void setBookName(String bn)
	{
		bookname=bn;
	}
	
        public String getAuthor()
	{
		return "作者: "+author;
	}
	public void setAuthor(String au)
	{
		author=au;
	}
	public String getPress()
	{
		return "出版社: "+press;
	}
	public void setPress(String pr)
	{
		press=pr;
	}
	public String getCopyLightDate()
	{
		return "版权日期: "+copylightdate;
	}
	public void setCopyLightDate(String cld)
	{
		copylightdate=cld;
	}
	public String toString()
	{
	return bookname+"\n"+author+"\n"+press+"\n"+copylightdate;
	}
	}

	
	
//-------------------20172308周亚杰------------------------------------
