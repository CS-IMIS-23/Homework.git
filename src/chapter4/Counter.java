    public class Counter
{
	public int number;

	public Counter()
	{
		number=0;
	}
	public int click()
	{
		return number=number+1;
	}
	public int getCount()
	{
		return number;
	}
	public int reset()
	{
		return number=0;
	}
	public String toString()
	{
		String result=Integer.toString(number);
		return result;
	}

}
