    public class Sphere
{
	public double diameter,area,volume;
	public Sphere()
	{
		diameter=5;
		
	}
	public double getDiameter()
	{
		return diameter;
	}
	public double setDiameter(double num)
	{
		diameter=num;
		return diameter;
	}
	public double volume()
	{
		volume=Math.PI*Math.pow(diameter,3)/6.0;
		return volume;
	}
	public double area()
	{
		area=Math.PI*Math.pow(diameter,2);
		return area;
	}
}



