package chapter4;

import java.util.Random;

public class PP00 {
    public static void main(String[] args) {
        int PseudoNumber;
        String num1,num2;

        Random generator=new Random();
        PseudoNumber=generator.nextInt(20)-10;
        System.out.println("产生的随机数为："+PseudoNumber);

        num1=Integer.toBinaryString(PseudoNumber);
        System.out.println("对应二进制为："+num1);

        num2=Integer.toHexString(PseudoNumber);
        System.out.println("对应十六进制为："+num2);
    }
}
