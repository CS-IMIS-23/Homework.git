package chapter13;

import chapter10.Sorting;

import java.util.ArrayList;

public class PP13_3 {
    public static void main(String[] args) {
        FormList form = new FormList();

        form.add(1);
        form.add(5);
        form.add(7);
        form.add(4);

        System.out.println(form);

        System.out.println("排序后： ");
        form.contact();
    }

    public static class FormList {
        private FormNode list;

        public FormList() {
            list = null;
        }


        public void add(int a) {
            FormNode node = new FormNode(a);
            FormNode current;

            if (list == null)
                list = node;
            else {
                current = list;
                while (current.next != null)
                    current = current.next;
                current.next = node;
            }

        }

        public void contact() {
            int n = 0;
            ArrayList<Comparable> magazines = new ArrayList();//------------将所有的节点元素存在ArrayList中
            while (list != null) {
                magazines.add(n, (Comparable) String.valueOf(list.num));
                list = list.next;
                n++;
            }//--------------------------------------------------------------------
            n = 0;
            Comparable[] list = new Comparable[magazines.size()];//将ArrayList中元素放入Comparable类型数组中
            while (n < magazines.size()) {
                list[n] = magazines.get(n);
                n++;
            }//------------------------------------------------------------------------------------
            Sorting.selectionSort(list);//对数组进行排序
            for (Comparable a : list)//输出排序结果
                System.out.println(a);
        }

        public String toString()
        {
            String result = "";

            FormNode current = list;

            while (current != null)
            {
                result += current.num + "\n";
                current = current.next;
            }

            return result;
        }
    }

    public static class FormNode {
        public int num;
        public FormNode next;

        public FormNode(int a) {
            num = a;
            next = null;
        }
    }

}
