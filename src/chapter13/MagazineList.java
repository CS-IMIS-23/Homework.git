package chapter13;//*******************************************************************
//  MagazineList.java       Author: Lewis/Loftus
//
//  Represents a collection of magazines.
//*******************************************************************

import chapter10.Sorting;
import chapter10.Sorting2;

import java.util.ArrayList;

public class MagazineList {
   public  MagazineNode list;

   //----------------------------------------------------------------
   //  Sets up an initially empty list of magazines.
   //----------------------------------------------------------------
   public MagazineList() {
      list = null;
   }

   //----------------------------------------------------------------
   //  Creates a new MagazineNode object and adds it to the end of
   //  the linked list.
   //----------------------------------------------------------------
   public void add(Magazine mag) {
      MagazineNode node = new MagazineNode(mag);
      MagazineNode current;

      if (list == null)
         list = node;
      else {
         current = list;
         while (current.next != null)
            current = current.next;
         current.next = node;
      }
   }

   public void insert(int index, Magazine newMagazine) {
      MagazineNode node = new MagazineNode(newMagazine);
      MagazineNode current = list;

      if (index == 0) {
         list = node;
         list.next = current;//插入为头元素
      } else {
         int i = 0;
         while (node != null && i < index - 1) {
            //查找到第index-1个元素
            current = current.next;
            i++;
         }
         node.next = current.next;
         current.next = node;
         }
   }//在index的位置插入新节点newMagazine

   public void delete(Magazine delNode) {
      MagazineNode node = new MagazineNode(delNode);// 待删除节点元素
      MagazineNode current = list;//current:待删除节点前元素

      if (String.valueOf(current.magazine).equals(String.valueOf(delNode))) {
         list = current.next;
      }//删除节点元素为第一个时

      else

   {
      while (!String.valueOf(current.next.magazine).equals(String.valueOf(delNode))) {
         current = current.next;

      }
      current.next = current.next.next;
   }//删除节点不是第一个节点时
}
//删除节点delNode

   public void contact()
   {
      int n =0;
      ArrayList<Comparable> magazines = new ArrayList();//------------将所有的节点元素存在ArrayList中
      while (list!=null)
      {
         magazines.add(n, (Comparable) String.valueOf(list.magazine));
         list = list.next;
         n++;
      }//--------------------------------------------------------------------
      n = 0;
      Comparable[] list = new Comparable[magazines.size()];//将ArrayList中元素放入Comparable类型数组中
      while (n<magazines.size())
      {
         list[n] = magazines.get(n);
         n++;
      }//------------------------------------------------------------------------------------
      Sorting.selectionSort(list);//对数组进行排序
//      for (Comparable a : list)//输出排序结果
//         System.out.print(a+" ");
   }


   //----------------------------------------------------------------
   //  Returns this list of magazines as a string.
   //----------------------------------------------------------------
   public String toString()
   {
      String result = "";

      MagazineNode current = list;

      while (current != null)
      {
         result += current.magazine + " ";
         current = current.next;
      }

      return result;
   }

   //*****************************************************************
   //  An inner class that represents a node in the magazine list.
   //  The public variables are accessed by the MagazineList class.
   //*****************************************************************
   public  class MagazineNode
   {
      public Magazine magazine;
      public MagazineNode next;

      //--------------------------------------------------------------
      //  Sets up the node
      //--------------------------------------------------------------
      public MagazineNode(Magazine mag)
      {
         magazine = mag;
         next = null;
      }
   }
}
