package chapter13;//********************************************************************
//  Movies.java       Author: Lewis/Loftus
//
//  Demonstrates the use of an array of objects.
//********************************************************************


public class Movies
{
   //-----------------------------------------------------------------
   //  Creates a DVDCollection object and adds some DVDs to it. Prints
   //  reports on the status of the collection.
   //-----------------------------------------------------------------
   public static void main(String[] args)
   {
      DVDCollection movies = new DVDCollection();

      movies.addDVD(new DVD("11","11",11,11,true));
      movies.addDVD(new DVD("12","12",12,12,true));
      movies.addDVD(new DVD("13","13",13,13,true));

      System.out.println(movies);
   }
}
