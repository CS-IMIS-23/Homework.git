package chapter9;

public class AnimalTest {
    public static void main(String[] args) {

        Cow cow = new Cow("牛牛", 2017);
        Sheep sheep = new Sheep("羊羊", 2018);

        cow.introduction();
        cow.eat();
        cow.sleep();

        System.out.println();

        sheep.introduction();
        sheep.eat();
        sheep.sleep();

    }
}
