package chapter9;

public abstract class Animal {
    private String name;
    private int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }
    public abstract void eat();
    public abstract void sleep();
    public abstract void introduction();
}
