package chapter9;

import chapter8.Book2;

public class Dictionary2 extends Book2 {
    private int definations;

    public Dictionary2(int numpages,int numDefinations)
    {
        super (numpages);
        definations=numDefinations;
    }
    public double computeRatio()
    {
        return (double)definations/pages;
    }
    public void setDefinations(int numDefinations)
    {
        definations=numDefinations;
    }
    public int getDefinations()
    {
        return definations;
    }
}
