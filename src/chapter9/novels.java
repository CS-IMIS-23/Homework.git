package chapter9;

public class novels
{
    int pages;
    String keywords;
    public novels(int page,String keyword)
    {
        pages=page;
        keywords=keyword;
    }
    public int getPages()
    {
        return pages;
    }

    public String getKeywords() {
        return keywords;
    }
    public String toString()
    {
        return "details: \n"+"页数："+pages+" 关键字："+keywords;
    }
}
