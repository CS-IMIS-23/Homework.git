package chapter9;

public class Cow extends Animal{
    public Cow(String name, int id) {
        super(name, id);
    }
    public void eat()
    {
        System.out.println("正在吃草");
    }
    public void sleep()
    {
        System.out.println("正在睡觉");
    }
    public void introduction()
    {
        System.out.println("大家好，我叫牛牛");
    }
}

