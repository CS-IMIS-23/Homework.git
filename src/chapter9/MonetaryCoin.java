package chapter9;

import java.util.Random;
import chapter5.Coin;
public class MonetaryCoin extends Coin{
    int[] side={110,5,10};
    int value;
    Random generator=new Random();

    public MonetaryCoin()
    {
        super.flip();
    }
    public int flips()
    {
        value=side[generator.nextInt(3)];
        return value;
    }
    public int getSide()
    {
        return value;
    }

}
